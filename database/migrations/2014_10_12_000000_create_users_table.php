<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->string('gender')->nullable();
            $table->string('team_id')->nullable();
            $table->string('team_member_id')->nullable();                        
            $table->string('phone')->nullable();
	        $table->string('linkedin')->nullable();
            $table->string('birth_date')->nullable();
            $table->string('status')->nullable(); //Status sedang kerja apa kaga (Employed,Unemployed,Student)
            $table->string('institution')->nullable(); //
            $table->string('position')->nullable();
            $table->string('role')->nullable();
            $table->text('description')->nullable();
            $table->boolean('payment')->default(false);
            $table->rememberToken()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
