<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Team;
use Carbon\Carbon;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('guest',['except'=>'destroy']);
    }

    public function index()
    {
        return view('index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $team_id = Team::find(session('id'));
        // if(!empty($team_id)){
        //     return view('add');
        // }else{  
        //     return redirect('/');
        // }
        return view('add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate(request(),[
            'name' => 'required',
            'email' => 'required',
            'password' => 'required|confirmed',
            'gender' => 'required',
            'birth_date' => 'required',
            'role' => 'required',
            'phone' => 'required',
            'status' => 'required',
            'description' => 'required',
            'linkedin' => 'required',
        ]);

        $name=$request->name;
        $email=$request->email;
        $password = $request->password;
        $gender=$request->gender;
        $institution=$request->institution;
        $position = $request->position;
        $status = $request->status;
        $description = $request->description;
        $birth_date = $request->birth_date;
        $role = $request->role;
        $phone = $request->phone;
        $linkedin = $request->linkedin; 

        $now = Carbon::now('utc')->toDateTimeString();
        
        $member = new User();
        $member->name = $name;
        $member->gender = $gender;
        $member->birth_date = $birth_date;
        $member->description = $description;
        $member->email = $email;
        $member->institution = $institution;
        $member->position = $position; 
        $member->status = $status; 
        $member->password = bcrypt($password);  
        $member->phone = $phone;
        $member->linkedin = $linkedin;
        $member->role = $role;
        $member->created_at=$now;
        $member->updated_at=$now;         
        $member->save();
        $user = User::latest()->first();
  
        Auth::loginUsingId($user->id);
        return redirect('/home');

    }

    public function login(Request $request){
        if(auth()->attempt(request(['email','password']))){
            $user = Auth::user();
            if($user->role == "Admin"){
                Session::put('Admin',"true");
                return redirect('/admin');
            }
            return redirect('/home');            
        }
        return redirect()->back();
        
    }   

    public function loginView(){
        return view('login');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        auth()->logout();
        return redirect('/');
    }

    public function viewEditPassword(){
        return view('editPassword');
    }

    public function updatePassword(Request $request, $id){
        $user = User::find($id);
        $user->update(['password'=>bcrypt($request->password)]);
        return redirect('loginView');
    }

    public function editPassword(Request $request){
        $user = User::where('email',$request->email)->where('name',$request->name)->first();
        if (!$user) {
           return view('editPassword')->with('Error','Error ! Data Not Found');
        }
        return view('updatePassword',compact('user'));
    }
}
