<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Information;
use App\user;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user= User::find(Auth::user()->id);
        
        return view('user-panel.user-profile',compact('user'));
    }

    public function info(){
        $informations = Information::orderBy('created_at','desc')->get();
        $user= User::find(Auth::user()->id);
        return view('user-panel.user-information',compact('informations','user'));
    }

    public function team(){
        $user= User::find(Auth::user()->id);
        return view('user-panel.user-team',compact('user'));
    }

}
