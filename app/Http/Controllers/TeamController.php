<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;
use App\User;
use App\TeamMember;
use Auth;
use Session;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        // $team_id = Team::find(session('id'));
        // if(session('quantity') && !empty($team_id)){
        //     return redirect('member/create');
        // }else{  
        //     return view('addTeam');
        // }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Create Team
        $this->validate($request, [
            'name' => 'required|max:255',
            'quantity' => 'required|numeric|min:0|max:3',
            'password' => 'required|max:255',
        ]);

        $team = $request->all();
        $team['unique_code'] = $this->randomNumber(5);
        Team::create($team);
        $team = Team::latest()->first();
        TeamMember::create(['user_id'=>Auth::user()->id,'team_id'=>$team->id]);
        $teamMember = TeamMember::latest()->first();
        Auth::user()->update(['team_id'=>$team->id,'team_member_id'=>$teamMember->id]);
        return redirect('home/team');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $team = Team::findOrfail($id);
        $team->update($request->all());
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::where('team_id',$id)->update(['team_id'=>NULL]);
        TeamMember::where('team_id',$id)->delete();
        Team::find($id)->delete();
        return redirect()->back();  
    }

    public function randomNumber($length) {
        $result = '';

        for($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }

    public function register_team(Request $request){
        $check = Team::where('name',$request->name)->where('unique_code',$request->unique_code)->where('password',$request->password)->first();

        if(count($check)){
            
            $checkQtyTeam = count($check->members);
            // dd($checkQtyTeam);
            if($checkQtyTeam >= $check->quantity){
                return redirect()->back()->with('errorQuantity','Sorry Team '.$check->name.' is Full !');
            }

            $flag = 1;

            for($i = 0; $i<count($check->members);$i++){
                if($check->members[$i]->userData->id == Auth::user()->id){
                    $flag = 0;
                    break;
                }
            }
        

            if($flag==1){
                TeamMember::create(['user_id'=>Auth::user()->id,'team_id'=>$check->id]);
                $teamMember = TeamMember::latest()->first();
                Auth::user()->update(['team_member_id'=>$teamMember->id]);
            }

            return redirect()->back();

        }else{
            return redirect()->back()->with('errorRegister','Sorry, Wrong Credentials ! Try Again !');
        }
    }
}
