<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;
use App\User;
use App\Information;
use App\TeamMember;
use Auth;
use Session;
use Carbon\Carbon;
use DB; 

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function __contruct(){
    //     $this->middleware('AdminMiddleware');
    // }
//tidak bsa login
    public function login(Request $request){
        if(!auth()->attempt(request(['email','password']))){
            return back();
        }
        Session::put('Admin','true');
        return redirect('/admin');        
    }

    public function logout()
    {
        Session::flush();
        auth()->logout();
        return redirect('/');
    }

    public function loginView()
    {
        return view('admin.admin-login');
    }


    public function index()
    {
        $teams = Team::all();
        return view('admin.admin',compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $informations = Information::all();
        return view('admin.add-announcement',compact('informations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $information = $request->all();
        Information::create($information);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function verify($id){
        
        $teams = User::findOrFail($id);
        $teams['payment']= TRUE;
        $teams->save();

        return redirect()->back();
    }

    public function participant_info(){
        // $participants = DB::table('users')->orderByRaw('-team_member_id DESC')->get();
        $participants = User::all();
        return view('admin.admin-participants',compact('participants'));
    }

    public function participant_delete($id){
        $user = User::findOrfail($id);
        if(count($user->userTeam)){
            $leader = $user->userTeam->team->leader;

            if($user->id == $leader->id){
                $team = $leader->team_id;
                TeamMember::where('user_id',$user)->delete();
                $user->delete();
                TeamMember::where('team_id',$team)->delete();
                Team::find($team)->delete();
            }
                TeamMember::find('user_id',$user)->delete();
                $user->delete();   
        }else{
            User::find($id)->delete();
        }
        
        return redirect()->back();
    }

    public function participant_update(Request $request, $id){
        $this->validate(request(),[
            'name' => 'required',
            'email' => 'required',
            'gender' => 'required',
            'birth_date' => 'required',
            'status' => 'required',
            'phone' => 'required',
            'linkedin' => 'required',
        ]);
        // echo "success";
        // $name=$request->name;
        // $email=$request->email;
        // $gender=$request->gender;
        // $age = $request->age;
        // $institution=$request->institution;
        // $position = $request->position;
        // $status = $request->status;
        // $role = $request->role;
        // $phone = $request->phone;
        // $linkedin = $request->linkedin; 

        $now = Carbon::now('utc')->toDateTimeString();
        
        $user = User::findOrfail($id);
        $user->update($request->all());
        return redirect()->back();
    }

}
