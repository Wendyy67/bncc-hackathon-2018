<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamMember extends Model
{
    protected $fillable = [
        'user_id','team_id'
    ];

    public function team(){
    	return $this->belongsTo(Team::Class);
    }

    public function userData(){
    	return $this->hasOne(User::Class);
    }
}
