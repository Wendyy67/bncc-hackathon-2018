<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = [
        'name', 'quantity','user_id','password','unique_code'
    ];

    protected $hidden = ['payment'];

    public function members(){
    	return $this->hasMany(TeamMember::Class);
    }

    public function leader(){
    	return $this->hasOne(User::Class);
    }
}
