<html>
	<head>
    <title>BNCC Hackathon</title>
	<link rel="icon" href="{{asset('resources/assets/img/logo-hackathon.png')}}">
	    <link rel="stylesheet" media="all" href="{{asset('resources/assets/css/admin.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fontawesome.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fa-brands.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fa-regular.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fa-solid.min.css')}}">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

  	</head>
  	<body>
    	<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
      		<nav id="sidebar">
    			<div id="sidebar-scroll">
        			<div class="sidebar-content">
            			<div class="side-header side-content bg-white-op" style="background-color: white;">
               			<button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                    		<i class="far fa-times"></i>
                		</button>
		                <a class="h5 text-white" href="{{url('/')}}">
							<img src="{{asset('resources/assets/img/logo-hackathon-small.png')}}" class="img-responsive">
						</a>            
						</div>
	            		<div class="side-content side-content-full">
			                <ul class="nav-main">
			                    <li>
			                      	<a href="{{route('user-information')}}">
			                        	<i class="far fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Information</span>
									</a>                   
								</li>
			                    <li>
			                     	<a href="{{route('user-profile')}}">
			                        	<i class="far fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Profile</span>
									</a>
								</li>
			                    <li>
			                     	<a href="{{route('user-team')}}">
			                        	<i class="far fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Team</span>
									</a>
								</li>
			                </ul>
	            		</div>
        			</div>
    			</div>
			</nav>

      		<header id="header-navbar" class="content-mini content-mini-full">

		  	<ul class="nav-header pull-right">
		  		<li>
		    		<a rel="nofollow" data-method="delete" href="{{url('/logout')}}">Log out</a>
		  		</li>
			</ul>
			</header>

			<main id="main-container">

				@if($user->payment == false)
				
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<strong>Still not verified (Waiting List)</strong><br><br>
							{{-- If you are Binusian (Free), Please send your Binusian Card to Our Contact Person ASAP. If you are from other university , Please Complete Payment of {{$user->name}} with the payment of Rp. 
							<b>{{number_format(50000)}}</b> / person
							to Bank Account BCA 527-1647-477 A/N Kevin Setiadi Gunawan ASAP ! <b> --}}
							<b>Please Contact the Contact Person before payment for confirmation of your team registration</b> 
					</div>
				@else

				@endif

		    <div class="content bg-gray-lighter">
			    <div class="row items-push">
			        <div class="col-sm-7">
			            <h1 class="page-heading">
			            	Welcome , {{$user->name}} !
			            </h1>
			        </div>
			        <div class="col-sm-5 text-right hidden-xs">
			        	Status : 
			        	@if($user->payment == true)
			        	<span style="color: #2ecc71;">Verified</span>
			        	@else
			        	<span style="color: #e74c3c">Waiting for Payment</span>
			        	@endif
			        </div>
			    </div>
			</div>


			<div class="content">

			@foreach($informations as $info)
			  <div class="row">
			    <div class="col-lg-12">
			        <div class="block ">
			            <div class="block-content">
			            <h2 style="margin-bottom: 20px;">{{$info->title}}&nbsp;&nbsp;&nbsp;<small style="color: #e74c3c;">New</small></h2> 
						<p style="text-align: justify;">{{$info->content}}
						</p>
			            </div>
			        </div>
			    </div>
			  </div>
			  @endforeach	

			  <div class="row">
			    <div class="col-lg-12">
			        <div class="block">
			            <div class="block-content">
			            <h2 style="margin-bottom: 20px;">About Hackathon</h2> 
						<p style="text-align: justify;">
							BNCC Hackathon 2018 merupakan serangkaian acara kompetisi di bidang IT yang diselenggarakan oleh Bina Nusantara Computer Club (BNCC). Pada tahun ini BNCC Hackathon hadir dengan mengangkat tema “Code Your Future”. BNCC Hackathon ingin mengajak seluruh masyarakat di Indonesia untuk bereksplorasi dan bertindak secara kreatif dalam memberikan solusi terhadap masalah-masalah yang sedang dihadapi pada era modern ini melalui inovasi yang berbasis teknologi digital. BNCC Hackathon juga ingin menjadi event yang mampu memberikan wadah dan sarana bagi masyarakat Indonesia khususnya yang memiliki keahlian dalam bidang pemograman untuk terjun langsung menghadapi masalah-masalah yang dihadapi di kehidupan nyata sekarang ini.
						</p>
			            </div>
			        </div>
			    </div>
			  </div>

				<div class="row">
				    <div class="col-lg-12">
				        <div class="block">
				            <div class="block-content">
				            <h2 style="margin-bottom: 20px;" class="text-center">About Organizer</h2> 
				            <a href="http://bncc.net"><img src="{{asset('resources/assets/img/logo-BNCC.png')}}" class="img-responsive" width="50%" style="margin: 0 auto;"></a>
							<p style="text-align: justify;margin-top: 20px;">
								Bina Nusantara Computer Club (BNCC) merupakan satu-satunya unit kegiatan mahasiswa yang berbasiskan komputer di Universitas Bina Nusantara yang telah berdiri sejak tahun 1989. Sebagai organisasi non-profit yang telah berdiri selama 28 tahun, BNCC memiliki 3 produk yaitu software house FAVESOLUTION, online media FileMagz.com dan kelas pembelajaran Learning and Training. Selain itu BNCC juga mempunyai External Event Organizer, Overclocking Community, dan Technopreneur Community.
							</p>
				            </div>
				        </div>
				    </div>
			  	</div>			

{{-- 			<div class="row">
				<div class="col-sm-12">
					<div class="block">
						<div class="block-content">
							<h2>Sponsors</h2>
							<img src="{{asset('resources/assets/img/Sponsors.png')}}" class="img-responsive">
					</div>
				</div>
			</div> --}}

			</main>

			<footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
				<div class="pull-left">
				BNCC Hackathon © <span class="js-year-copy"></span>
				</div>
			</footer>
   		</div>

    <script type="text/javascript" src="{{asset('resources/assets/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('resources/assets/js/bootstrap.min.js')}}"></script>
  </body>
</html>
