
<html>
  <head>
    <title>BNCC Hackathon</title>
	<link rel="icon" href="{{asset('resources/assets/img/logo-hackathon.png')}}">

    <link rel="stylesheet" media="all" href="{{asset('resources/assets/css/admin.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fontawesome.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fa-brands.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fa-regular.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fa-solid.min.css')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
  </head>
  <body>
    	<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
      		<nav id="sidebar">
    			<div id="sidebar-scroll">
        			<div class="sidebar-content">
            			<div class="side-header side-content bg-white-op" style="background-color: white;">
               			<button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                    		<i class="fa fa-times"></i>
                		</button>
		                <a class="h5 text-white" href="{{url('/')}}">
							<img src="{{asset('resources/assets/img/logo-hackathon-small.png')}}" class="img-responsive">
						</a>            
						</div>
	            		<div class="side-content side-content-full">
			                <ul class="nav-main">
			                    <li>
			                      	<a href="{{route('user-information')}}">
			                        	<i class="far fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Information</span>
									</a>                   
								</li>
			                    <li>
			                     	<a href="{{route('user-profile')}}">
			                        	<i class="far fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Profile</span>
									</a>
								</li>
			                    <li>
			                     	<a href="{{route('user-team')}}">
			                        	<i class="far fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Team</span>
									</a>
								</li>
			                </ul>
	            		</div>
        			</div>
    			</div>
			</nav>

      		<header id="header-navbar" class="content-mini content-mini-full">

		  	<ul class="nav-header pull-right">
		  		<li>
		    		<a rel="nofollow" data-method="delete" href="{{url('/logout')}}">Log out</a>
		  		</li>
			</ul>
			</header>

			<main id="main-container">

				@if($user->payment == false)
				
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<strong>Still not verified (Waiting List)</strong><br><br>
							{{-- If you are Binusian (Free), Please send your Binusian Card to Our Contact Person ASAP. If you are from other university , Please Complete Payment of {{$user->name}} with the payment of Rp. 
							<b>{{number_format(50000)}}</b> / person
							to Bank Account BCA 527-1647-477 A/N Kevin Setiadi Gunawan ASAP ! <b> --}}
							<b>Please Contact the Contact Person before payment for confirmation of your team registration</b>
					</div>
				@else

				@endif

		    <div class="content bg-gray-lighter">
			    <div class="row items-push">
			        <div class="col-sm-7">
			            <h1 class="page-heading">
			            	Welcome , {{$user->name}} !
			            </h1>
			            	@if(count($user->team))
				            	@if($user->id == $user->team->leader_user_id)
				            	<h3>Team Leader</h3>
				            	@endif
				            @endif
			        </div>
			        <div class="col-sm-5 text-right hidden-xs">
			        	Status : 
			        	@if($user->payment == true && $user->id<73)
			        		<span style="color: #2ecc71;">Verified</span>
			        	@elseif($user->payment == false && $user->id<73)
			        		<span style="color: #e74c3c">Waiting for Payment</span>
			        	@elseif($user->id>=73 || $user->id == 1)
			        		<span style="color: #e74c3c">Currently listed in <b>Waiting List</b></span>
			        	@endif

{{-- 			        	@if($user->userTeam->team->id == 14 || $user->userTeam->team->id == 16 || $user->userTeam->team->id == 19 || $user->userTeam->team->id == 22)
			        		<span style="color: #e74c3c">Your team ({{$user->userTeam->team->name}}) currently listed in<b>Waiting List</b></span>
			        	@endif --}}
			        	
			        </div>
			    </div>
			</div>


			<div class="content">
			  <div class="row">
			    <div class="col-lg-12">
			        <div class="block">
			            <div class="block-content">
			            @if(count($user->userTeam))
			            	<h2>Team {{$user->userTeam->team->name}}</h2> 

			            @else
			            	<a style="text-decoration: none;" href="{{url('home/team')}}"><h2>No Team</h2>
			            @endif
			 			<div class="row" style="margin-top: 20px;">

			 				@if(count($user->userTeam))
			 					
				 				@for($i = 0; $i<count($user->userTeam->team->members);$i++)

								<a data-toggle="modal" href='#{{$user->userTeam->team->members[$i]->userData['id']}}data' style="text-decoration: none;"><div class="col-sm-4 text-center" style="padding: 20px;">
									<img src="{{asset('resources/assets/img/Developer.png')}}" style="margin: 0 auto;" class="img-responsive">
									<h3>{{$user->userTeam->team->members[$i]->userData['name']}}</h3>
									<h5>{{$user->userTeam->team->members[$i]->userData['role']}}</h5>
								</div>
								</a>

								@endfor

							@else

								<a data-toggle="modal" href='#{{$user->id}}data' style="text-decoration: none;"><div class="col-sm-4 text-center" style="padding: 20px;">
									<img src="{{asset('resources/assets/img/Developer.png')}}" style="margin: 0 auto;" class="img-responsive">
									<!-- buat desain untuk yang developer,desainer dan bisnis -->
									<h3>{{$user->name}}</h3>
									<h5>{{$user->role}}</h5>
								</div>
								</a>

							@endif
						</div>
			       </div>
			    </div>
			  </div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<div class="block">
						<div class="block-content">
							<h2 style="margin-bottom: 20px;">Rules of Hackathon</h2>
							<p style="text-align: justify;">You can access the following link for information regarding the hackathon rules. If You have question , you can send mail to technocareer@bncc.net<br><br><a href="#">Link Coming Soon !</a>
							</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="block">
						<div class="block-content">
							<h2 style="margin-bottom: 20px;">Contact Person</h2>
							<div>
								<p style="text-align: justify;">
									Got questions about hackathon? Feel Free to Contact the details below for more information
								</p>
								<h4><i class="far fa-user-circle"></i>&nbsp;&nbsp;&nbsp;&nbsp;Aldrian Kwan</h4>
								<h4><i class="fas fa-phone"></i>&nbsp;&nbsp;&nbsp;&nbsp;082112567451 (Whatsapp)</h4>
								<h4><i class="far fa-envelope"></i>&nbsp;&nbsp;&nbsp;&nbsp;technocareer@bncc.net</h4>

							</div>
						</div>
					</div>
				</div>
			</div>

{{-- 			<div class="row">
				<div class="col-sm-12">
					<div class="block">
						<div class="block-content">
							<h2>Sponsors</h2>
							<img src="{{asset('resources/assets/img/Sponsors.png')}}" class="img-responsive">
						</div>
					</div>
				</div>
			</div> --}}
	</main>

    <footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
		<div class="pull-left">
	        BNCC Hackathon © <span class="js-year-copy"></span>
	    </div>
	</footer>

    </div>

@if(count($user->userTeam))

@for($i = 0; $i<count($user->userTeam->team->members);$i++)
 	<div class="modal fade" id="{{$user->userTeam->team->members[$i]->userData['id']}}data">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Data of {{$user->userTeam->team->members[$i]->userData['name']}}</h4>
				</div>
				<div class="modal-body">
					<table class="table table-striped table-hover text-center">
						<thead">
							<tr>
								<th class="text-center">Information</th>
								<th class="text-center">Information</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Name</td>
								<td>{{$user->userTeam->team->members[$i]->userData['name']}}</td>
							</tr>
							<tr>
								<td>Gender</td>
								<td>{{$user->userTeam->team->members[$i]->userData['gender']}}</td>
							</tr>
							<tr>
								<td>Birth Date</td>
								<td>{{$user->userTeam->team->members[$i]->userData['birth_date']}}</td>
							</tr>
							<tr>
								<td>Email</td>
								<td>{{$user->userTeam->team->members[$i]->userData['email']}}</td>
							</tr>
							<tr>
								<td>Status</td>
								<td>{{$user->userTeam->team->members[$i]->userData['status']}}</td>
							</tr>
							<tr>
								<td>Institution</td>
								<td>{{$user->userTeam->team->members[$i]->userData['institution']}}</td>
							</tr>
							<tr>
								<td>Linkedin</td>
								<td>{{$user->userTeam->team->members[$i]->userData['linkedin']}}</td>
							</tr>
							<tr>
								<td>Position</td>
								<td>{{$user->userTeam->team->members[$i]->userData['position']}}</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
@endfor

@else

  	<div class="modal fade" id="{{$user->id}}data">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Data of {{$user->name}}</h4>
				</div>
				<div class="modal-body">
					<table class="table table-striped table-hover text-center">
						<thead">
							<tr>
								<th class="text-center">Information</th>
								<th class="text-center">Information</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Name</td>
								<td>{{$user->name}}</td>
							</tr>
							<tr>
								<td>Gender</td>
								<td>{{$user->gender}}</td>
							</tr>
							<tr>
								<td>Birth Date</td>
								<td>{{$user->birth_date}}</td>
							</tr>
							<tr>
								<td>Email</td>
								<td>{{$user->email}}</td>
							</tr>
							<tr>
								<td>Status</td>
								<td>{{$user->status}}</td>
							</tr>
							<tr>
								<td>Institution</td>
								<td>{{$user->institution}}</td>
							</tr>
							<tr>
								<td>Linkedin</td>
								<td>{{$user->linkedin}}</td>
							</tr>
							<tr>
								<td>Position</td>
								<td>{{$user->position}}</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

@endif

    <script type="text/javascript" src="{{asset('resources/assets/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('resources/assets/js/bootstrap.min.js')}}"></script>
  </body>
</html>
