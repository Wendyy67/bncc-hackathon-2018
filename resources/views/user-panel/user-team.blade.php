<html>
	<head>
    <title>BNCC Hackathon</title>
	<link rel="icon" href="{{asset('resources/assets/img/logo-hackathon.png')}}">
	    <link rel="stylesheet" media="all" href="{{asset('resources/assets/css/admin.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fontawesome.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fa-brands.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fa-regular.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fa-solid.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/panel.css')}}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

  	</head>
  	<body>
    	<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
      		<nav id="sidebar">
    			<div id="sidebar-scroll">
        			<div class="sidebar-content">
            			<div class="side-header side-content bg-white-op" style="background-color: white;">
               			<button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                    		<i class="far fa-times"></i>
                		</button>
		                <a class="h5 text-white" href="{{url('/')}}">
							<img src="{{asset('resources/assets/img/logo-hackathon-small.png')}}" class="img-responsive">
						</a>            
						</div>
	            		<div class="side-content side-content-full">
			                <ul class="nav-main">
			                    <li>
			                      	<a href="{{route('user-information')}}">
			                        	<i class="far fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Information</span>
									</a>                   
								</li>
			                    <li>
			                     	<a href="{{route('user-profile')}}">
			                        	<i class="far fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Profile</span>
									</a>
								</li>
			                    <li>
			                     	<a href="{{route('user-team')}}">
			                        	<i class="far fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Team</span>
									</a>
								</li>
			                </ul>
	            		</div>
        			</div>
    			</div>
			</nav>

      		<header id="header-navbar" class="content-mini content-mini-full">

		  	<ul class="nav-header pull-right">
		  		<li>
		    		<a rel="nofollow" data-method="delete" href="{{url('/logout')}}">Log out</a>
		  		</li>
			</ul>
			</header>

			<main id="main-container">

				@if($user->payment == false)
				
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<strong>Still not verified (Waiting List)</strong><br><br>
							{{-- If you are Binusian (Free), Please send your Binusian Card to Our Contact Person ASAP. If you are from other university , Please Complete Payment of {{$user->name}} with the payment of Rp. 
							<b>{{number_format(50000)}}</b> / person
							to Bank Account BCA 527-1647-477 A/N Kevin Setiadi Gunawan ASAP ! <b> --}}
							<b>Please Contact the Contact Person before payment for confirmation of your team registration</b>
					</div>
				@else

				@endif

		    <div class="content bg-gray-lighter">
			    <div class="row items-push">
			        <div class="col-sm-7">
			            <h1 class="page-heading">
			            	Welcome , {{$user->name}} !
			            </h1>
			        </div>
			        <div class="col-sm-5 text-right hidden-xs">
			        	Status : 
			        	@if($user->payment == true)
			        	<span style="color: #2ecc71;">Verified</span>
			        	@else
			        	<span style="color: #e74c3c">Waiting for Payment</span>
			        	@endif
			        </div>
			    </div>
			</div>

			@if(session('errorQuantity'))
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Error</strong> <br>{{session('errorQuantity')}}
				</div>
			@elseif(session('errorRegister'))
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Error</strong><br> {{session('errorRegister')}}
				</div>
			@endif

			<div class="content">
			  <div class="row">
			    <div class="col-lg-12">
			        <div class="block ">
			            <div class="block-content">

			            	@if(count($user->userTeam))
{{-- 			            		Current Team : {{\App\Team::find($user->team_id)['name']}} <br>
			            		Team Unique Code : {{\App\Team::find($user->team_id)['unique_code']}} <br>
								Team Leaders : {{\App\Team::find($user->team_id)['leader_user_id']}} <br>
								Current Members : {{count(\App\Team::find($user->team_id)->users)}} --}}
								Current Team : <b>{{$user->userTeam->team->name}}</b> <br>
								Team Unique Code : <b>{{$user->userTeam->team->unique_code}}</b> <br>
								Team Leaders : <b>{{$user->userTeam->team->leader->name}}</b> <br>
								Current Members : <b>{{count($user->userTeam->team->members)}}</b> <br>
			            	@else
			            		Current Team : 
			            		<div style="margin-top: 10px">
				            			<a class="btn btn-success" data-toggle="modal" href='#create-team-form'>Create Team</a>
				            			<a  class="btn btn-primary" data-toggle="modal" href='#join-team-form'>Join Team</a>
				            	</div>
			            	@endif

			            </div>
			        </div>
			    </div>
			  </div>
		
{{-- 			<div class="row">
				<div class="col-sm-12">
					<div class="block">
						<div class="block-content">
							<h2>Sponsors</h2>
							<img src="{{asset('resources/assets/img/Sponsors.png')}}" class="img-responsive">
					</div>
				</div>
			</div> --}}

			</main>

			<footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
				<div class="pull-left">
				BNCC Hackathon © <span class="js-year-copy"></span>
				</div>
			</footer>
   		</div>

		<div class="modal fade" id="create-team-form">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Create Team</h4>
					</div>
					<div class="modal-body">
				        <form class="form-horizontal" method="POST" action="{{url('team')}}">
				            {{csrf_field()}}
				            <div class="input-wrapper">
				                    <input type="text" class="inputclass" name="name"  placeholder="Team Name" required autofocus>
				            </div>

				            <div class="no-bd-top input-wrapper">
				                    <input type="number" class="inputclass" name="quantity" placeholder="Team Quantity" max="3" required>
				            </div>

				            <div class="no-bd-top input-wrapper">
				                    <input type="password" class="inputclass" name="password" placeholder="Team Password" required>
				            </div>

				            <div class="no-bd-top input-wrapper">
				                    <input type="password" class="inputclass" id="password_confirmation" name="password_confirmation" placeholder="Team Password Confirmation" required>
				            </div>

				            <div class="container-button">
				                    <button type="submit" class="button-class">
				                        Register Team
				                    </button>
				            </div>
				        </form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="join-team-form">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Join Team</h4>
					</div>
					<div class="modal-body">
				        <form class="form-horizontal" method="POST" action="{{url('team/registerTeam')}}">
				            {{csrf_field()}}
				            <div class="input-wrapper">
				                <input type="text" class="inputclass" name="name"  placeholder="Team Name" required autofocus>
				            </div>

				            <div class="no-bd-top input-wrapper">
				                    <input type="text" class="inputclass" name="unique_code" placeholder="Team Unique Code" required>
				            </div>


				            <div class="no-bd-top input-wrapper">
				                    <input type="password" class="inputclass" name="password" placeholder="Team Password" required>
				            </div>

				            <div class="container-button">
				                    <button type="submit" class="button-class">
				                        Submit Team
				                    </button>
				            </div>
				        </form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

    <script type="text/javascript" src="{{asset('resources/assets/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('resources/assets/js/bootstrap.min.js')}}"></script>
  </body>
</html>


