<html>
	<head>
    <title>BNCC Hackathon</title>
		<link rel="icon" href="{{asset('resources/assets/img/logo-hackathon.png')}}">
	    <link rel="stylesheet" media="all" href="{{asset('resources/assets/css/admin.css')}}"/>
	    <link rel="stylesheet" media="all" href="{{asset('resources/assets/css/panel.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fontawesome.min.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fa-brands.min.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fa-regular.min.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fa-solid.min.css')}}">
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
  	</head>
  	<body>
    	<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
      		<nav id="sidebar">
    			<div id="sidebar-scroll">
        			<div class="sidebar-content">
            			<div class="side-header side-content bg-white-op" style="background-color: white;">
               			<button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                    		<i class="fa fa-times"></i>
                		</button>
		                <a class="h5 text-white" href="{{url('/')}}">
							<img src="{{asset('resources/assets/img/logo-hackathon-small.png')}}" class="img-responsive">
						</a>            
						</div>
	            		<div class="side-content side-content-full">
			                			                <ul class="nav-main">
			                    <li>
			                      	<a href="{{url('admin')}}">
			                        	<i class="fa fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Team Data</span>
									</a>                   
								</li>
			                    <li>
			                      	<a href="{{url('admin/participant')}}">
			                        	<i class="fa fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Participants Data</span>
									</a>                   
								</li>
			                    <li>
			                     	<a href="{{url('admin/create')}}">
			                        	<i class="fa fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Add Announcement</span>
									</a>
								</li>
			                </ul>
	            		</div>
        			</div>
    			</div>
			</nav>

      		<header id="header-navbar" class="content-mini content-mini-full">

		  	<ul class="nav-header pull-right">
		  		<li>
		    		<a rel="nofollow" data-method="delete" href="{{url('/admin/logout')}}">Log out</a>
		  		</li>
			</ul>
			</header>

			<main id="main-container">


		    <div class="content bg-gray-lighter">
			    <div class="row items-push">
			        <div class="col-sm-7">
			            <h1 class="page-heading">
			            	Welcome , Admin BNCC !
			            </h1>
			        </div>
			        <div class="col-sm-5 text-right hidden-xs">
			        </div>
			    </div>
			</div>

					@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif

			<div class="content">
			  <div class="row">
			    <div class="col-lg-12">
			        <div class="block">
			            <div class="block-content" style="overflow-x: auto;">
              				<table id="tableParticipant" class="table table-responsive table-striped table-borderless table-header-bg">
                  			<thead>
	    						<tr>
							        <th class="text-center">No.</th>	
							        <th class="">Name</th>
							        <th class="">Email</th>
							        <th class="">BirthDate</th>
							        <th class="">Team</th>
							        <th class="">Phone</th>
							        <th class="">Linkedin</th>
							        <th class="">Status</th>
							        <th class="">Institution</th>
							        <th class="">Position</th>
							        <th class="">Payment</th>
		        					<th class="text-center">Action</th>
	    						</tr>
							</thead>

							<tbody>
								@foreach($participants as $key => $user)
								@if($user->role=='Admin')

								@else
		                        <tr>
		                            <td class="text-center">{{$key++}}</td>
		                            <td>
		                            	<a data-toggle="modal" href='#{{$user->id}}user'>{{$user->name}}
		                            	</a>
		                            </td>
		                            <td>
		                            	{{$user->email}}
		                            </td>
		                            <td>
		                            	{{$user->birth_date}}
		                            </td>
		                            @if(count($user->userTeam))
									<td>{{$user->userTeam->team['name']}}</td>
									@else
									<td>No Team</td>
									@endif
		                            <td>
		                            	{{$user->phone}}
		                            </td>
		                            <td>
		                            	{{$user->linkedin}}
		                            </td>
		                            <td>
		                            	{{$user->status}}
		                            </td>
		                            <td>
		                            	{{$user->institution}}
		                            </td>
		                            <td>
		                            	{{$user->position}}
		                            </td>
		                            <td>
		                            	@if($user->payment == true)
		                            		<span style="color: #2ecc71;">Paid</span>
		                            	@else
		                            		<span style="color: #e74c3c">Not Paid</span>
		                            	@endif		                            
		                            </td>
		                            <td class="text-center">
		                                <div class="btn-group">
		                                	<form method="GET" action="{{url('admin/member/delete/'.$user->id)}}" id="data-delete{{$user->id}}">
												{{csrf_field()}}
		                                		<input type="hidden" name="_method" value="DELETE">
		                                	<a class="btn btn-xs btn-default" data-toggle="modal" href='#{{$user->id}}verifyuser'>
			                                    <i class="fas fa-check"></i>
											</a>
		                                    <a class="btn btn-xs btn-default" data-toggle="modal" href='#{{$user->id}}user'>
		                                    	<i class="fas fa-pencil-alt"></i>
											</a>
		                                    <a class="btn btn-xs btn-default" data-toggle="modal" href='#modal-confirm-delete{{$user->id}}'>
		                                     	<i class="fas fa-times"></i>
											</a> 

											</form>                           
										</div>
		                            </td>
		                        </tr>
		                        @endif
		                        @endforeach
							</tbody>
			              </table>
			            </div>
			        </div>
			    </div>
			  </div>		

{{-- 			<div class="row">
				<div class="col-sm-12">
					<div class="block">
						<div class="block-content">
							<h2>Sponsors</h2>
							<img src="{{asset('resources/assets/img/Sponsors.png')}}" class="img-responsive">
					</div>
				</div>
			</div> --}}

			</main>

			<footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
				<div class="pull-left">
				BNCC Hackathon © <span class="js-year-copy"></span>
				</div>
			</footer>
   		</div>

   		@foreach($participants as $user)
   		<div class="modal fade" id="{{$user->id}}user">
   			<div class="modal-dialog">
   				<div class="modal-content">
   					<div class="modal-header">
   						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   						<h4 class="modal-title">Participant {{$user->name}}</h4>
   					</div>
   					<div class="modal-body">

						<form action="{{url('admin/member/update/'.$user->id)}}" method="GET" role="form">
						{{csrf_field()}}
						<input type="hidden" name="_method" value="PATCH">
						<div class="input-wrapper">
							<input type="text" name="name" class="inputclass" id="" placeholder="Input Name" value="{{$user->name}}" required>
						</div>

						<input type="hidden" name="team_id" value="{{$user->team_id}}">
						
						<div class="no-bd-top input-wrapper">
							<input type="email" name="email" class="inputclass" id="" placeholder="Input Email" value="{{$user->email}}" required>
						</div>

						<div class="no-bd-top input-wrapper">
							<select name="gender" class="inputclass" required="required">
			  					<option class="inputclass" value="" selected disabled hidden>Input Gender</option>
								<option class="inputclass" @if($user->gender == 'Male') selected @endif value="Male">Male </option>
								<option class="inputclass" @if($user->gender == 'Female') selected @endif value="Female">Female</option>
							</select>
						</div>

						<div class="no-bd-top input-wrapper">
							<input type="type" name="birth_date" class="inputclass" id="" placeholder="Input BirthDate" value="{{$user->birth_date}}" required>
						</div>

						<div class="no-bd-top input-wrapper">
							<input type="text" name="phone" class="inputclass" id="" placeholder="Input Phone Number" value="{{$user->phone}}" required>
						</div>

						<div class="no-bd-top input-wrapper">
							<select name="role" id="inputRole[]" class="inputclass" required="required">
			  					<option class="inputclass" value="" selected disabled hidden>Input Hackathon Role</option>
								<option class="inputclass" @if($user->role == 'Hustler') selected @endif  value="Hustler">Huster (Marketing) </option>
								<option class="inputclass" @if($user->role == 'Hipster') selected @endif disabled=" " value="Hipster">Hipster (Designer) </option>
								<option class="inputclass" @if($user->role == 'Hacker') selected @endif  value="Hacker">Hacker (Developer) </option>
							</select>
						</div>

						<div class="no-bd-top input-wrapper">
							<select name="status" id="inputRole[]" class="inputclass" required="required">
			  					<option class="inputclass" value="" selected disabled hidden>Input Current Status</option>
								<option class="inputclass" @if($user->status == 'Unemployed') selected @endif  value="Unemployed">Unemployed</option>
								<option class="inputclass" @if($user->status == 'Employed') selected @endif  value="Employed">Employed </option>
								<option class="inputclass" @if($user->status == 'Student') selected @endif  value="Student">Student</option>
							</select>
						</div>

						<div class="no-bd-top input-wrapper">
							<input type="text" name="linkedin" class="inputclass id="" placeholder="Input Linkedin" value="{{$user->linkedin}}" required>
						</div>

						<div class="no-bd-top input-wrapper">
							<input type="text" name="institution" class="inputclass id="" placeholder="Input Institution" value="{{$user->institution}}">
						</div>

						<div class="no-bd-top input-wrapper">
							<input type="text" name="position" class="inputclass" id="" placeholder="Input Position" value="{{$user->position}}">
						</div>

						<div class="container-button-regis col-sm-4 col-sm-offset-4" style="margin-top: 20px;">
						<button type="submit" class="button-class">Submit</button>
						</div>

						</form>

   					</div>
   					<div class="modal-footer">
   						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
   					</div>
   				</div>
   			</div>
   		</div>
   		@endforeach

   		@foreach($participants as $user)
   		<div class="modal fade" id="modal-confirm-delete{{$user->id}}">
   			<div class="modal-dialog">
   				<div class="modal-content">
   					<div class="modal-header">
   						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   						<h4 class="modal-title">Confirm Delete</h4>
   					</div>
   					<div class="modal-body">
   						Confirm delete {{$user->name}} from BNCC Hackathon ? 
   					</div>
   					<div class="modal-footer">
   						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
   						<button type="button" class="btn btn-danger" id="modal-confirmed-delete{{$user->id}}">Delete Data</button>
   					</div>
   				</div>
   			</div>
   		</div>

   		<div class="modal fade" id="{{$user->id}}verifyuser">
   			<div class="modal-dialog">
   				<div class="modal-content">
   					<div class="modal-header">
   						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   						<h4 class="modal-title">Confirm Verify</h4>
   					</div>
   					<div class="modal-body">
   						Confirm {{$user->name}} has paid Rp. {{number_format(50000)}} to BNCC ? 
   					</div>
   					<div class="modal-footer">
   						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<a id="link-confirm-verify{{$user->id}}" href="{{url('admin/participant/verify/'.$user->id)}}" class="btn btn-success" type="button">Verify</a>
   					</div>
   				</div>
   			</div>
   		</div>

   		@endforeach



    <script type="text/javascript" src="{{asset('resources/assets/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('resources/assets/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css">">	
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>

    <script type="text/javascript">
    	$(document).ready(function(){    	
    		$('#tableParticipant').DataTable();
			var date_input=$('input[name="birth_date"]'); //our date input has the name "date"
			var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
			var options={
			    format: 'mm/dd/yyyy',
			    container: container,
			    todayHighlight: true,
			    autoclose: true,
			    };
			date_input.datepicker(options);
	    	@foreach($participants as $user)
	    		$("#modal-confirmed-delete{{$user->id}}").click(function(){
	    			$('#data-delete{{$user->id}}').submit();
	    		});
	    	@endforeach
    	});
    </script>

  </body>
</html>


