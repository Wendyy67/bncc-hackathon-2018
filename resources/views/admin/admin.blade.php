	<html>
	<head>
    <title>BNCC Hackathon</title>
	<link rel="icon" href="{{asset('resources/assets/img/logo-hackathon.png')}}">s

	    <link rel="stylesheet" media="all" href="{{asset('resources/assets/css/admin.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fontawesome.min.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fa-brands.min.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fa-regular.min.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fa-solid.min.css')}}">
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">

  	</head>
  	<body>
    	<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
      		<nav id="sidebar">
    			<div id="sidebar-scroll">
        			<div class="sidebar-content">
            			<div class="side-header side-content bg-white-op" style="background-color: white;">
               			<button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                    		<i class="far fa-times"></i>
                		</button>
		                <a class="h5 text-white" href="{{url('/')}}">
							<img src="{{asset('resources/assets/img/logo-hackathon-small.png')}}" class="img-responsive">
						</a>            
						</div>
	            		<div class="side-content side-content-full">
							<ul class="nav-main">
			                    <li>
			                      	<a href="{{url('admin')}}">
			                        	<i class="far fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Team Data</span>
									</a>                   
								</li>
			                    <li>
			                      	<a href="{{url('admin/participant')}}">
			                        	<i class="far fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Participants Data</span>
									</a>                   
								</li>
			                    <li>
			                     	<a href="{{url('admin/create')}}">
			                        	<i class="far fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Add Announcement</span>
									</a>
								</li>
			                </ul>
	            		</div>
        			</div>
    			</div>
			</nav>

      		<header id="header-navbar" class="content-mini content-mini-full">

		  	<ul class="nav-header pull-right">
		  		<li>
		    		<a rel="nofollow" data-method="delete" href="{{url('/admin/logout')}}">Log out</a>
		  		</li>
			</ul>
			</header>

			<main id="main-container">


		    <div class="content bg-gray-lighter">
			    <div class="row items-push">
			        <div class="col-sm-7">
			            <h1 class="page-heading">
			            	Welcome , Admin BNCC !
			            </h1>
			        </div>
			        <div class="col-sm-5 text-right hidden-xs">
			        </div>
			    </div>
			</div>


			<div class="content">
			  <div class="row">
			    <div class="col-lg-12">
			        <div class="block">
			            <div class="block-content">
              				<table id="tableTeam" class="table table-responsive table-striped table-borderless table-header-bg">
                  			<thead>
	    						<tr>
							        <th class="text-center">No.</th>	
							        <th class="">Team</th>
							        <th class="">Quantity</th>
							        <th class="">Unique Code</th>
		        					<th class="text-center">Actions</th>
	    						</tr>
							</thead>

							<tbody>
								@foreach($teams as $key => $team)
		                        <tr>
		                            <td class="text-center">{{++$key}}</td>
		                            <td>
		                            	<a data-toggle="modal" href='#{{$team->id}}team' @if(count($team->members)==3) style="color: #2ecc71;" @endif>{{$team->name}}</a>
		                            </td>
									<td>{{$team->quantity}} Members </td>
		                            <td>
		                            	{{$team->unique_code}}
		                            </td>
		                            <td class="text-center">
		                                <div class="btn-group">
											<form method="POST" action="{{url('team/'.$team->id)}}" id="data-delete{{$team->id}}">
												{{csrf_field()}}
												<input type="hidden" name="_method" value="DELETE">
		                                		<a data-toggle="modal" class="btn btn-xs btn-default" href='#edit{{$team->id}}'>
		                                			<i class="fas fa-pencil-alt"></i>
		                                		</a>

			                                    <a data-toggle="modal" href='#confirm-delete{{$team->id}}' class="btn btn-xs btn-default">
			                                     <i class="fas fa-times"></i>
												</a> 
											</form>                               
										</div>
		                            </td>
		                        </tr>
		                        @endforeach
							</tbody>
			              </table>
			            </div>
			        </div>
			    </div>
			  </div>		

{{-- 			<div class="row">
				<div class="col-sm-12">
					<div class="block">
						<div class="block-content">
							<h2>Sponsors</h2>
							<img src="{{asset('resources/assets/img/Sponsors.png')}}" class="img-responsive">
					</div>
				</div>
			</div> --}}

			</main>

			<footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
				<div class="pull-left">
				BNCC Hackathon © <span class="js-year-copy"></span>
				</div>
			</footer>
   		</div>

   		@foreach($teams as $team)
   		<div class="modal fade" id="{{$team->id}}team">
   			<div class="modal-dialog">
   				<div class="modal-content">
   					<div class="modal-header">
   						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   						<h4 class="modal-title">Team {{$team->name}}</h4>
   					</div>
   					<div class="modal-body">

              				<table class="table table-responsive table-striped table-borderless table-header-bg">
                  			<thead>
	    						<tr>
							        <th class="text-center">No.</th>	
							        <th class="">Name</th>
							        <th class="">Role</th>
							        <th class="">Status</th>
							        <th class="">Payment</th>
							        <th class="">Institution</th>
	    						</tr>
							</thead>

							<tbody>
								@if(count($team->members)>0)

									@for($i = 0; $i<count($team->members);$i++)

				                        <tr>
				                            <td class="text-center">{{$i+1}}</td>

				                            <td>@if($team->leader['id']==$team->members[$i]->userData['id'])
				                            		<span style="color: #2ecc71;">{{$team->members[$i]->userData['name']}}</span>
				                            	@else
				                            		{{$team->members[$i]->userData['name']}}
				                            	@endif
				                            </td>
											<td>{{$team->members[$i]->userData['role']}}</td>
											<td>{{$team->members[$i]->userData['status']}}</td>
											<td>
				                            	@if($team->members[$i]->userData['payment'] == true)
				                            		<span style="color: #2ecc71;">Paid</span>
				                            	@else
				                            		<span style="color: #e74c3c">Not Paid</span>
				                            	@endif
											</td>
				                            <td>
				                            	{{$team->members[$i]->userData['institution']}}
				                            </td>
				                        </tr>
			                        @endfor

		                        @else

		                        @endif
							</tbody>
			              </table>

   					</div>
   					<div class="modal-footer">
   						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
   					</div>
   				</div>
   			</div>
   		</div>
   		@endforeach

   		@foreach($teams as $team)
   		<div class="modal fade" id="edit{{$team->id}}">
   			<div class="modal-dialog">
   				<div class="modal-content">
   					<div class="modal-header">
   						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   						<h4 class="modal-title">Modal title</h4>
   					</div>
   					<div class="modal-body">
   						<form action="{{url('team/'.$team->id)}}" method="POST" role="form">	
   							{{csrf_field()}}
   							<legend>Edit Team Name</legend>
   							<input type="hidden" name="_method" value="PATCH">
   							<div class="form-group">
   								<label for="">Team Name</label>
   								<input type="text" class="form-control" name="name" value="{{$team->name}}" id="" placeholder="Input New Team Name">
   							</div>
   						
   							<button type="submit" class="btn btn-primary">Submit</button>
   						</form>
   					</div>
   					<div class="modal-footer">
   						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
   					</div>
   				</div>
   			</div>
   		</div>
   		@endforeach

   		@foreach($teams as $team)
   		<div class="modal fade" id="confirm-delete{{$team->id}}">
   			<div class="modal-dialog">
   				<div class="modal-content">
   					<div class="modal-header">
   						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   						<h4 class="modal-title">Confirm Delete</h4>
   					</div>
   					<div class="modal-body">
   						Confirm delete team {{$team->name}}
   					</div>
   					<div class="modal-footer">
   						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
   						<button type="button" class="btn btn-danger" id="modal-confirm-delete{{$team->id}}">Delete</button>
   					</div>
   					<form action="{{url('team/'.$team->id)}}" method="GET" id="data-delete{{$team->id}}" role="form">
   						<input type="hidden" name="_method" value="DELETE">
   					</form>
   				</div>
   			</div>
   		</div>
   		@endforeach

    <script type="text/javascript" src="{{asset('resources/assets/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('resources/assets/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('resources/assets/js/confirmation.js')}}"></script>
    <script type="text/javascript" src="{{asset('resources/assets/js/popover.js')}}"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>

    <script type="text/javascript">
    	$(document).ready(function(){
    		$('#tableTeam').DataTable();
	    	@foreach($teams as $team)
	    		$("#modal-confirm-delete{{$team->id}}").click(function(){
	    			$('#data-delete{{$team->id}}').submit();
	    		});
	    	@endforeach
    	});
    </script>

  </body>
</html>
