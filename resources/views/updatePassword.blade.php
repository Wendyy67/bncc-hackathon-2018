<!DOCTYPE html>
<html>
<head>
    <title>BNCC Hackathon</title>
    <link rel="icon" href="{{asset('resources/assets/img/logo-hackathon.png')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/bootstrap.min.css')}}">  
    <link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/panel.css')}}">
</head>
<body class="body">

<div class="container container-wrapper">
    <div class="row">
        <a href="{{url('/')}}"><img src="{{asset('resources/assets/img/logo-hackathon-small.png')}}" class="img-responsive" style="margin: 0 auto;">
        </a>
        <h3 class="form-title">Generate New Password</h3>
        <form class="form-horizontal" method="POST" action="{{url('user/editPassword/'.$user->id)}}">
            {{ csrf_field() }}

            <div class="input-wrapper">
                <input type="password" name="password" class="inputclass" id="" placeholder="Input Password" required>
            </div>

            <div class="no-bd-top input-wrapper">
                <input type="password" name="password_confirmation" class="inputclass" id="password_confirmation" placeholder="Input Password Confirmation" required>
            </div>

            <div class="container-button">
                <button type="submit" class="button-class">
                    Change Password 
                </button>
            </div>
        </form>
    </div>
</div> 

    <script type="text/javascript" src="{{asset('resources/assets/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('resources/assets/js/bootstrap.min.js')}}"></script>
</body>
</html>