<!DOCTYPE html>
<html>
<head>
    <title>BNCC Hackathon</title>
	<link rel="icon" href="{{asset('resources/assets/img/logo-hackathon.png')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/bootstrap.min.css')}}">  
    <link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/panel.css')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body class="body">

<div class="container container-wrapper">
    <div class="row">
        <a href="{{url('/')}}"><img src="{{asset('resources/assets/img/logo-hackathon-small.png')}}" class="img-responsive" style="margin: 0 auto;">
        </a>
        <h3 class="form-title">Register Team</h3>
        <form class="form-horizontal" method="POST" action="{{url('team')}}">
            {{csrf_field()}}
            <div class="input-wrapper">
                    <input type="text" class="inputclass" name="name"  placeholder="Team Name" required autofocus>
            </div>

            <div class="no-bd-top input-wrapper">

                    <input type="number" class="inputclass" name="quantity" placeholder="Team Quantity" max="3" required>

            </div>

            <div class="container-button">
                    <button type="submit" class="button-class">
                        Register Team
                    </button>
            </div>
        </form>
    </div>
</div> 

    <div class="container-fluid text-center" style="position: fixed;left: 0;bottom: 0;width: 100%;padding: 10px;background-color: #4c4c4c;font-family: 'Montserrat';color: #ffffff;;">
        Question ? Please Contact : Aldrian (082112567451 WhatsApp) or technocareer@bncc.net 
    </div>

    <script type="text/javascript" src="{{asset('resources/assets/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('resources/assets/js/bootstrap.min.js')}}"></script>
</body>
</html>