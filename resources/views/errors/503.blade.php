<!DOCTYPE html>
<html>
<head>
    <title>BNCC Hackathon</title>
	<link rel="icon" href="{{asset('resources/assets/img/logo-hackathon.png')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/bootstrap.min.css')}}">  
    <link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/panel.css')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body class="body">

<div class="container container-wrapper">
    <div class="row">
        <div class="col-sm-12 text-center">
            <h2>Sorry, There is something wrong !</h2>
            <h4>{{ $exception->getMessage() }}</h3>
        </div>
    </div>
</div> 

    <script type="text/javascript" src="{{asset('resources/assets/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('resources/assets/js/bootstrap.min.js')}}"></script>
</body>
</html>