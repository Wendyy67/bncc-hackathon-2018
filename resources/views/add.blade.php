<!DOCTYPE html>
<html>
<head>
    <title>BNCC Hackathon</title>
    <link rel="icon" href="{{asset('resources/assets/img/logo-hackathon.png')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/bootstrap.min.css')}}">	
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/panel.css')}}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body class="body">

	<div class="container-fluid">

		<form action="{{url('member')}}" method="POST" role="form" id="form-registration">
			{{csrf_field()}}

			@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
			
			<div class="container-wrapper-fluid">
			<h3 class="form-title">Register Hackathon</h3>
			<div class="container text-center" style="margin-bottom: 30px;">

			{{-- <h3>Hackathon Registrant Form</h3> --}}

			<h4>Sorry, Registration is Closed !</45>
{{-- 			<div class="input-wrapper">
				<input type="text" name="name" class="inputclass" id="" placeholder="Input Name" required>
			</div>
			
			<div class="no-bd-top input-wrapper">
				<input type="email" name="email" class="inputclass" id="email_address0" placeholder="Input Email"required >
			</div>

			<div class="no-bd-top input-wrapper">
				<input type="password" name="password" class="inputclass" id="" placeholder="Input Password" required>
			</div>

			<div class="no-bd-top input-wrapper">
				<input type="password" name="password_confirmation" class="inputclass" id="password_confirmation" placeholder="Input Password Confirmation" required>
			</div>


			<div class="no-bd-top input-wrapper">
				<select name="gender" class="inputclass" required="required">
  					<option class="inputclass" value="" selected disabled hidden>Input Gender</option>
					<option class="inputclass" value="Male">Male</option>
					<option class="inputclass" value="Female">Female</option>
				</select>
			</div>

				<div class="no-bd-top input-wrapper">
					<input class="inputclass" required id="date" name="birth_date" placeholder="Input Birth Date" type="text">
				</div>

				<div class="no-bd-top input-wrapper">
					<input type="text" name="phone" class="inputclass" id="" placeholder="Input Phone Number [08]" pattern="[08][0-9]{9,}" max-length="12" required>
				</div>

				<div class="no-bd-top input-wrapper">
					<input type="text" name="linkedin" class="inputclass" id="" placeholder="Input Linkedin Account Link" required>
				</div>

				<div class="no-bd-top input-wrapper">
					<input type="text" name="referral_code" class="inputclass" id="" placeholder="Input Referral Code">
				</div>

				<div class="no-bd-top input-wrapper">
					<textarea class="inputclass" style="outline:none;min-height: 120px;padding:25px;" placeholder="Input About Yourself" name="description"></textarea>
				</div>

				<div class="no-bd-top input-wrapper">
					<select name="role" id="inputRole[]" class="inputclass" required="required">
	  					<option class="inputclass" value="" selected disabled hidden>Input Hackathon Role</option>
						<option class="inputclass" value="Hustler">Hustler (Marketing) </option>
						<option class="inputclass" value="Hipster">Hipster (Designer) </option>
						<option class="inputclass" value="Hacker">Hacker (Developer) </option>
					</select>
				</div>

				<div class="no-bd-top input-wrapper" id="status-wrapper0">
					<select id="status0" name="status" class="inputclass" required="required">
	  					<option class="inputclass" value="" selected disabled hidden>Input Current Status</option>
						<option class="inputclass" value="Unemployed">Unemployed</option>
						<option class="inputclass" value="Employed">Employed</option>
						<option class="inputclass" value="Student">Student</option>
					</select>
				</div>

			</div>
			<div class="container-button-regis col-sm-4 col-sm-offset-4" style="margin-top: 20px;">
			<button type="submit" class="button-class">Submit</button>
			</div> --}}

			</div>
		</form>
	</div>

	<div class="container-fluid text-center" style="position: fixed;left: 0;bottom: 0;width: 100%;padding: 10px;background-color: #4c4c4c;font-family: 'Montserrat';color: #ffffff;;">
		Question ? Please Contact : Aldrian (082112567451 WhatsApp) or technocareer@bncc.net 
	</div>

	<script type="text/javascript" src="{{asset('resources/assets/js/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('resources/assets/js/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css">">
	
	<script type="text/javascript">
   (function($) {
		var date_input=$('input[name="birth_date"]'); //our date input has the name "date"
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		var options={
		    format: 'mm/dd/yyyy',
		    container: container,
		    todayHighlight: true,
		    autoclose: true,
		    };
		date_input.datepicker(options);


		$('#status0').change(function(event){
			event.preventDefault();
			var status = $('#status0').val();
			var status_wrapper = $('#status-wrapper0');
			var employed_info1 = $('#employed-info10');
			var employed_info2 = $('#employed-info20');
			var student_info1 = $('#student-info10');
			var student_info2 = $('#student-info20');
			var university = $('#university0');
			var major = $('#major0');
			var institution = $('#institution0');
			var position = $('#position0');
			if(status=='Employed'){
				if(!university.length || !major.length || !position.length || !institution.length){
					employed_info1.remove();
					employed_info2.remove();
					student_info1.remove();
					student_info2.remove();
					status_wrapper.after("<div class='no-bd-top input-wrapper' id='employed-info10'><input type='text' name='institution' class='inputclass' id='institution0' placeholder='Input Institution' required></div><div class='no-bd-top input-wrapper' id='employed-info20'><input type='text' name='position' class='inputclass' id='positionO' placeholder='Input Position' required></div>");
				}
			}else if(status=='Student'){
				if(!university.length || !major.length || !position.length || !institution.length){
					employed_info1.remove();
					employed_info2.remove();
					student_info1.remove();
					student_info2.remove();
					status_wrapper.after("<div class='no-bd-top input-wrapper' id='student-info10'><input type='text' name='institution' class='inputclass' id='university0' placeholder='Input University' required></div><div class='no-bd-top input-wrapper' id='student-info20'><input type='text' name='position' class='inputclass' id='major0' placeholder='Input Major' required></div>");
				}
			}else if(status=='Unemployed'){
					employed_info1.remove();
					employed_info2.remove();
					student_info1.remove();
					student_info2.remove();
			}
		});

	})(jQuery);
	</script>

</body>
</html>