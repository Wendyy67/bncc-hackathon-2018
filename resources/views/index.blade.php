<!DOCTYPE html>
<html>
<head>
	<title>BNCC Hackathon</title>
	<meta name="title" content="BNCC Hackathon" />
	<meta name="description" content="BNCC Hackathon" />
	<link rel="icon" href="{{asset('resources/assets/img/logo-hackathon.png')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fontawesome.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fa-brands.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fa-regular.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/fa-solid.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/style.css')}}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body data-spy="scroll" data-target="#myScrollspy" data-offset="20">	

	<nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="myScrollspy">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><img src="{{asset('resources/assets/img/logo-hackathon-small.png')}}" class="img-responsive" width="200" height="auto"></a>
			</div>

			<div class="collapse navbar-collapse navbar-ex1-collapse">	
				<ul class="nav navbar-nav navbar-right">
					<li class="home-nav"><a href="#home-nav">Home</a></li>
					<li class="about-nav"><a href="#about-nav">About</a></li>
					<li class="faq-nav"><a href="#faq-nav">FAQ</a></li>
					<li class="sponsor-nav"><a href="#sponsor-nav">Sponsor</a></li>
					<li class="contact-nav"><a href="#contact-nav">Contact</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div>
	</nav>

	<div class="container-fluid header" id="home-nav">
		<div class="overlay" >
			<img src="{{asset('resources/assets/img/logo-hackathon.png')}}" class="header-logo img-responsive" >
			<div class="container overlay-text">
				<h1 class="header-title">Road to BNCC Techno Career</h1>
				<h3 class="header-description">Code Your Future</h3>
				<div class="container-button">
				<a href="{{url('loginView')}}">
					<button class="btn btn-custom btn-primary btn-lg col-sm-6" style="float: left;">Login</button>	
				</a>
				<a href="{{url('member/create')}}"><button class="btn btn-custom btn-primary btn-lg col-sm-6" style="float: right;">Register</button></a>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid about" id="about-nav">
		<h2 class="text-center title-about">BNCC Hackathon</h2>
		<h3 class="text-center small-title-about">Code Your Future</h3>
		<div class="row row-about">
			<div class="col-sm-12 about-description">
			BNCC Hackathon merupakan sebuah event yang mengajak para developer Indonesia untuk berkumpul di satu tempat selama 24 jam dan bersama-sama mensolusikan masalah-masalah yang sedang dihadapi pada era modern ini melalui inovasi yang berbasis teknologi digital. Tahun ini BNCC Hackathon mengusung tema "Code your Future", dimana BNCC Hackathon ingin membahas masalah-masalah seputar ketenagakerjaan yang ada di Indonesia. Tentu dengan tema tersebut para peserta akan diajak untuk berinovasi memberikan solusi aplikasi yang dapat diterapkan untuk membantu masyarakat Indonesia secara luas.
			BNCC Hackathon tidak hanya memberikan hadiah menarik kepada para pemenang, namun juga kesempatan bagi para pemenang untuk mendapatkan tawaran pekerjaan dari para perusahaan Startup maupun multinasional ternama di Indonesia.
			</div>
			<div class="col-sm-6">
				<img style="margin: 50px auto;" src="{{asset('resources/assets/img/Logo-Placeholder.png')}}" class="img-responsive" width="160" height="auto">
				<h3 class="text-center Logo-about-description">Tokopedia Tower (51st Floor)</h3>
			</div>
			<div class="col-sm-6">
				<img style="margin: 50px auto;" src="{{asset('resources/assets/img/Logo-Calendar.png')}}" class="img-responsive" width="160" height="auto">
				<h3 class="text-center Logo-about-description">12-13 May 2018</h3>
			</div>
{{-- 			<div class="col-sm-12">
				<h3 class="text-left">Deadline
				<span class="text-left-description">Registration 10 April 2018</span>
				</h3>
			</div> --}}
			<div class="col-sm-12">
				<h3 class="text-center title-about" style="margin-top: 50px;">Timeline BNCC Hackathon</h3>
				<table class="table table-hover" style="width: 80%;max-width: 100%;margin: 30px auto;">
					<thead style="background: #3498db;color: white;">
						<tr>
							<th>Date</th>
							<th>Activity</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><b>10 April</b></td>
							<td>Pendaftaran dibuka</td>
						</tr>		
						<tr>
							<td><b>5 Mei</b></td>
							<td>Penutupan registrasi , batas terakhir pembayaran dan technical Meeting tentang BNCC Hackathon</td>
						</tr>				
						<tr>
							<td><b>12-13 Mei</b></td>
							<td>Pelaksanaan BNCC Hackathon</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="container-fluid faq" id="faq-nav">
		<h2 class="text-center faq-title">Frequently Asked Question</h2>
		<div class="row" style="margin-top: 50px;">
			<div class="col-sm-offset-1 col-sm-5">
				<a data-toggle="collapse" href="#collapse1" style="text-decoration: none;" class="rotate-trigger">
				  <div class="panel-group">
				    <div class="panel panel-default">
				      <div class="panel-heading">
				        <h4 class="panel-title" style="font-size: 1.1em;">
				          Siapa saja yang bisa ikut berpartisipasi?
				          <img class="rotate" src="{{asset('resources/assets/img/arrow.png')}}" style="float:right;height: 20px;">
				        </h4>
				      </div>
				      <div id="collapse1" class="panel-collapse collapse">
				        <div class="panel-body">Semua orang dapat berpartisipasi</div>
				      </div>
				    </div>
				  </div>
				</a>
				<a data-toggle="collapse" href="#collapse2" style="text-decoration: none;" class="rotate-trigger">
				  <div class="panel-group">
				    <div class="panel panel-default">
				      <div class="panel-heading">
				        <h4 class="panel-title" style="font-size: 0.9em;">
				          Apakah saya boleh mengikuti hackathon sendiri atau dengan tim yang berjumlah kurang dari tiga orang?
				          <img class="arrow rotate" src="{{asset('resources/assets/img/arrow.png')}}">
				        </h4>
				      </div>
				      <div id="collapse2" class="panel-collapse collapse">
				        <div class="panel-body">Tidak. Jika kurang, anggota kelompok akan dirandom dan digabung dengan kelompok lain untuk melengkapi jumlah anggota yang sudah ditentukan (3 Orang).</div>
				      </div>
				    </div>
				  </div>
				</a>
				<a data-toggle="collapse" href="#collapse3" style="text-decoration: none;" class="rotate-trigger">
				  <div class="panel-group">
				    <div class="panel panel-default">
				      <div class="panel-heading">
				        <h4 class="panel-title" style="font-size: 1.1em;">
				          Apa yang ingin dicapai dari hackathon ini?
				          <img class="arrow rotate" src="{{asset('resources/assets/img/arrow.png')}}">
				        </h4>
				      </div>
				      <div id="collapse3" class="panel-collapse collapse">
				        <div class="panel-body">
						 Hackathon ini bertujuan untuk mencari talent-talent yang tersembunyi dan memberikan kesempatan kepada peserta untuk mendapatkan pekerjaan di perusahaan besar
				        </div>
				      </div>
				    </div>
				  </div>
				</a>
				<a data-toggle="collapse" href="#collapse4" style="text-decoration: none;" class="rotate-trigger">
				  <div class="panel-group">
				    <div class="panel panel-default">
				      <div class="panel-heading">
				        <h4 class="panel-title" style="font-size: 0.9em;">
				          Apa saja yang akan disediakan oleh panitia di Hackathon ini ?
				          <img class="arrow rotate" src="{{asset('resources/assets/img/arrow.png')}}">
				        </h4>
				      </div>
				      <div id="collapse4" class="panel-collapse collapse">
				        <div class="panel-body">Panitia sendiri akan menyediakan wifi, port untuk charging, proyektor beserta MIC dan speaker untuk presentasi, meja dan kursi. Untuk internet, para peserta sangat disarankan untuk membawa internet portable pribadi untuk menghindari hal-hal yang tidak diinginkan.</div>
				      </div>
				    </div>
				  </div>
				</a>
				<a data-toggle="collapse" href="#collapse8" style="text-decoration: none;" class="rotate-trigger">
				  <div class="panel-group">
				    <div class="panel panel-default">
				      <div class="panel-heading">
				        <h4 class="panel-title" style="font-size: 1.1em;">
				          Apa batasan pembuatan aplikasi dalam hackathon ini?
				          <img class="arrow rotate" src="{{asset('resources/assets/img/arrow.png')}}">
				        </h4>
				      </div>
				      <div id="collapse8" class="panel-collapse collapse">
				        <div class="panel-body">Aplikasi yang dibuat harus dalam bentuk website dan mobile application yang dapat berfungsi. Aplikasi yang akan dibuat harus sesuai dengan case yang akan diberikan saat technical meeting. Requirement teknis dalam aplikasi yang akan dibuat peserta akan diberikan saat hari-H.</div>
				      </div>
				    </div>
				  </div>
				</a>
				<a data-toggle="collapse" href="#collapse12" style="text-decoration: none;" class="rotate-trigger">
				  <div class="panel-group">
				    <div class="panel panel-default">
				      <div class="panel-heading">
				        <h4 class="panel-title" style="font-size: 1.1em;">
				         	Apakah peserta boleh menginap di Tokopedia Tower saat hackathon berlangsung ? 
				          <img class="arrow rotate" src="{{asset('resources/assets/img/arrow.png')}}">
				        </h4>
				      </div>
				      <div id="collapse12" class="panel-collapse collapse">
				        <div class="panel-body">Boleh, panitia tidak akan menyediakan perlengkapan menginap (matras atau sleeping bag). Diharapkan peserta membawa matras atau sleeping bag sendiri bila ingin menginap di Tokopedia Tower.</div>
				      </div>
				    </div>
				  </div>
				</a>
			</div>
			<div class="col-sm-5">
				<a data-toggle="collapse" href="#collapse9" style="text-decoration: none;" class="rotate-trigger">
				  <div class="panel-group">
				    <div class="panel panel-default">
				      <div class="panel-heading">
				        <h4 class="panel-title" style="font-size: 1.1em;">
				          Apa saja aspek penilaian lomba?
				          <img class="arrow rotate" src="{{asset('resources/assets/img/arrow.png')}}">
				        </h4>
				      </div>
				      <div id="collapse9" class="panel-collapse collapse">
				        <div class="panel-body">Yang akan dinilai adalah design, konsep, kodingan dan kesesuaian dengan case</div>
				      </div>
				    </div>
				  </div>
				</a>
				<a data-toggle="collapse" href="#collapse10" style="text-decoration: none;" class="rotate-trigger">
				  <div class="panel-group">
				    <div class="panel panel-default">
				      <div class="panel-heading">
				        <h4 class="panel-title" style="font-size: 1.1em;">
				          Berapa batas banyak tim yang terdaftar?
				          <img class="arrow rotate" src="{{asset('resources/assets/img/arrow.png')}}">
				        </h4>
				      </div>
				      <div id="collapse10" class="panel-collapse collapse">
				        <div class="panel-body">Jumlah tim dibatasi sebanyak 15-20 tim.</div>
				      </div>
				    </div>
				  </div>
				</a>
				<a data-toggle="collapse" href="#collapse5" style="text-decoration: none;" class="rotate-trigger">
				  <div class="panel-group">
				    <div class="panel panel-default">
				      <div class="panel-heading">
				        <h4 class="panel-title" style="font-size: 0.9em;">
				          Apakah akan ada sesi mentoring selama hackathon berlangsung ?  
				          <img class="arrow rotate" src="{{asset('resources/assets/img/arrow.png')}}">
				        </h4>
				      </div>
				      <div id="collapse5" class="panel-collapse collapse">
				        <div class="panel-body">
				        	Ada , akan ada 3 jenis sesi mentoring yaitu mentor bidang teknologi (coding) , mentor bidang desain dan mentor bidang konsep aplikasi 
				        </div> 
				      </div>
				    </div>
				  </div>
				</a>
				<a data-toggle="collapse" href="#collapse6" style="text-decoration: none;" class="rotate-trigger">
				  <div class="panel-group">
				    <div class="panel panel-default">
				      <div class="panel-heading">
				        <h4 class="panel-title" style="font-size: 1.1em;">
				          Apakah karya yang sebelumnya pernah dilombakan boleh dilombakan di lomba ini? 
				          <img class="arrow rotate" src="{{asset('resources/assets/img/arrow.png')}}">
				        </h4>
				      </div>
				      <div id="collapse6" class="panel-collapse collapse">
				        <div class="panel-body">Tidak boleh, dikarenakan BNCC Hackathon ingin mencari ide-ide kreatif yang orisinil dalam perlombaan ini. Hasil dari hackathon akan menjadi hak milik BNCC dan Sponsor BNCC Hackathon.
				        </div>
				      </div>
				    </div>
				  </div>
				</a>
				<a data-toggle="collapse" href="#collapse7" style="text-decoration: none;" class="rotate-trigger">
				  <div class="panel-group">
				    <div class="panel panel-default">
				      <div class="panel-heading">
				        <h4 class="panel-title" style="font-size: 1.1em;">
				         	Saya berasal dari luar Jakarta/Jawa, apakah panitia menyediakan transport dan tempat menginap?
				          <img class="arrow rotate" src="{{asset('resources/assets/img/arrow.png')}}">
				        </h4>
				      </div>
				      <div id="collapse7" class="panel-collapse collapse">
				        <div class="panel-body">Tidak, transport dan tempat menginap akan ditanggung oleh masing-masing kelompok</div>
				      </div>
				    </div>
				  </div>
				</a>
				<a data-toggle="collapse" href="#collapse11" style="text-decoration: none;" class="rotate-trigger">
				  <div class="panel-group">
				    <div class="panel panel-default">
				      <div class="panel-heading">
				        <h4 class="panel-title" style="font-size: 1.1em;">
				         	Seberapa lama peserta diperbolehkan membuat aplikasi dalam Hackathon ini?
				          <img class="arrow rotate" src="{{asset('resources/assets/img/arrow.png')}}">
				        </h4>
				      </div>
				      <div id="collapse11" class="panel-collapse collapse">
				        <div class="panel-body">Peserta akan memiliki <b>7 hari setelah technical meeting untuk menyusun konsep aplikasi </b> untuk mensolusikan case yang telah diberikan saat technical meeting dan waktu pembuatan aplikasi (coding) adalah 25 jam selama hackathon ini berlangsung.</div> 
				      </div>
				    </div>
				  </div>
				</a>
			</div>
	</div>
</div>

	<div class="container-fluid sponsors" id="sponsor-nav">
		<h2 class="text-center title-about color-black">Platinum Sponsors</h2>
		<div class="row" style="margin-top: 50px;padding: 0 50px;">
			<div class="col-sm-offset-3 col-sm-3 img-wrapper">
				<a href="https://www.grab.com/id/" target="_blank"><img src="{{asset('resources/assets/img/platinum/grab-min.png')}}" class="img-responsive" style="margin: 0 auto;"></a>
			</div>
			<div class="col-sm-3 img-wrapper">
				<a href="https://kudo.co.id/" target="_blank"><img src="{{asset('resources/assets/img/platinum/kudo-min.png')}}" class="img-responsive" style="margin: 0 auto;"></a>
			</div>

		</div>

		<h2 class="text-center title-about color-black">Gold Sponsors</h2>
		<div class="row" style="margin-top: 50px;">
			<div class="col-sm-3 img-wrapper">
				<a href="https://www.bukalapak.com" target="_blank"><img src="{{asset('resources/assets/img/gold/bukalapak-min.png')}}" class="img-responsive" style="margin: 0 auto;"></a>
			</div>
			<div class="col-sm-3 img-wrapper">
				<a href="https://www.jobs.id/" target="_blank"><img src="{{asset('resources/assets/img/gold/jobsid-min.png')}}" class="img-responsive" style="margin: 0 auto;"></a>
			</div>
			<div class="col-sm-3 img-wrapper">
				<a href="https://www.tokopedia.com/" target="_blank"><img src="{{asset('resources/assets/img/gold/tokopedia-min.png')}}" class="img-responsive" style="margin: 0 auto;"></a>
			</div>
			<div class="col-sm-3 img-wrapper">
				<a href="https://www.ovo.id/" target="_blank"><img src="{{asset('resources/assets/img/gold/ovo-min.png')}}" class="img-responsive" style="margin: 0 auto;"></a>
			</div>
		</div>

		<h2 class="text-center title-about color-black">Silver Sponsors</h2>
		<div class="row" style="margin-top: 50px;">
			<div class="col-sm-3 img-wrapper">
				<a href="http://www.adata.com/id/" target="_blank"><img src="{{asset('resources/assets/img/silver/adata-min.png')}}" class="img-responsive" style="margin: 0 auto;"></a>
			</div>
			<div class="col-sm-3 img-wrapper">
				<a href="https://www.dewaweb.com/" target="_blank"><img src="{{asset('resources/assets/img/silver/dewaweb-min.png')}}" class="img-responsive" style="margin: 0 auto;"></a>
			</div>
			<div class="col-sm-3 img-wrapper">
				<a href="https://ottencoffee.co.id/" target="_blank"><img src="{{asset('resources/assets/img/silver/ottencoffee-min.png')}}" class="img-responsive" style="margin: 0 auto;"></a>
			</div>
			<div class="col-sm-3 img-wrapper">
				<a href="https://hacktiv8.com/" target="_blank"><img src="{{asset('resources/assets/img/silver/hacktiv8-min.png')}}" class="img-responsive" style="margin: 0 auto;"></a>
			</div>
		</div>

		<h2 class="text-center title-about color-black">Media Partner</h2>
		<div class="row" style="margin-top: 50px;padding: 0 50px;">
			<div class="col-sm-12 img-wrapper">
				<img src="{{asset('resources/assets/img/mediapartner/Medpart-min.png')}}" class="img-responsive" style="margin: 0 auto;">
			</div>
		</div>

	</div>

	<div class="container-fluid contact" id="contact-nav">
		<h2 class="text-center title-about color-white" style="margin-bottom: 30px;">Contact Us</h2>
		<div class="row row-about">
			<div class="col-sm-6">
				<div id="map" style="height: 300px;max-width: 100%;margin-bottom: 30px;"></div>
			</div>
				<div class="col-sm-offset-2 col-sm-4">
					<div class="container-divider">
						<h4><i class="far fa-envelope"></i>&nbsp;&nbsp;technocareer@bncc.net</h4>
					</div>
					<div class="container-divider">
						<h4><i class="fab fa-instagram"></i>&nbsp;&nbsp;<a style="text-decoration: none; color: #ffffff;" href="https://www.instagram.com/bnccbinus/?hl=en">BNCCBinus</a></h4>
					</div>
					<div class="container-divider">
						<h4><i class="fab fa-facebook"></i>&nbsp;&nbsp;<a style="text-decoration: none;color:#ffffff;" href="https://web.facebook.com/bina.nusantara.computer.club/?hc_ref=NEWSFEED">BNCCBinus</a></h4>
					</div>
					<div class="container-divider">
						<h4><i class="fab fa-whatsapp"></i>&nbsp;&nbsp;+6282112567451 (Aldrian)</h4>
					</div>
					<div class="container-divider">
						<h4><i class="fab fa-line"></i>&nbsp;&nbsp;@joy0117j</h4>
					</div>
				</div>
		</div>
	</div> 

	<div class="container-fluid organizer">
		<h2 class="text-center title-about color-black">About the Organizer</h2>
		<a href="http://bncc.net"><img src="{{asset('resources/assets/img/logo-BNCC.png')}}" class="img-responsive organizer-logo" style="margin: 40px auto;"></a>
		<div class="row row-about">
			<div class="col-sm-12 about-description">
				Bina Nusantara Computer Club (BNCC) merupakan satu-satunya unit kegiatan mahasiswa yang berbasiskan komputer di Universitas Bina Nusantara yang telah berdiri sejak tahun 1989. Sebagai organisasi non-profit yang telah berdiri selama 28 tahun, BNCC memiliki 3 produk yaitu software house <a href="http://favesolution.com">FAVESOLUTION</a>, online media <a href="http://filemagz.com">FileMagz.com</a> dan kelas pembelajaran Learning and Training. Selain itu BNCC juga mempunyai External Event Organizer, Overclocking Community, dan Technopreneur Community.
			</div>
		</div>
	</div>

	<div class="container-fluid coming-soon">
		<h2 class="text-center title-about color-white" style="margin-bottom: 30px;">More Information to come ! <br><small class="color-white">Stay tune in BNCC Social Media !</small></h2>
		<div class="row row-about">
			<div class="col-sm-12 about-description text-center color-white">
				For sponsor partnership, please contact <b>sponsor@bncc.net</b><br>
				For media partner partnership, please contact <b>partnership@bncc.net</b>
			</div>
		</div>
	</div> 

	<div class="container-fluid footer text-center">
		Copyright of BNCC 2018
	</div>

	<script type="text/javascript" src="{{asset('resources/assets/js/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('resources/assets/js/bootstrap.min.js')}}"></script>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBz68JnzqctOUuL0McMOk5TX7e-J_aMwZ8&callback=initMap">
	</script>
	<script type="text/javascript">
		function initMap() {
	        var uluru = {lat: -6.214059, lng: 106.818811};
	        var map = new google.maps.Map(document.getElementById('map'), {
	          zoom: 17,
	          center: uluru
	        });
	        var marker = new google.maps.Marker({
	          position: uluru,
	          map: map
	        });
	     };

		$(".navbar-brand,.home-nav").click(function (event){
			event.preventDefault();
	        $('html, body').animate({
	            scrollTop: 0
	        }, 800,'swing');
	     });
		$(".about-nav").click(function (event){
			event.preventDefault();
	        $('html, body').animate({
	            scrollTop: 620
	        }, 800,'swing');
	     });
		$(".faq-nav").click(function (event){
			event.preventDefault();
	        $('html, body').animate({
	            scrollTop: 1500
	        }, 800,'swing');
	     });
		$(".sponsor-nav").click(function (event){
			event.preventDefault();
	        $('html, body').animate({
	            scrollTop: 2200
	        }, 800,'swing');
	     });
		$(".contact-nav").click(function (event){
			event.preventDefault();
	        $('html, body').animate({
	            scrollTop: 3100
	        }, 800,'swing');
	     });
	</script>
	<script type="text/javascript">
		@if(session('Login-Alert')){
			alert("{{session('Login-Alert')}}")
		}
		@endif
	</script>
</body>
</html>

