<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::resource('/member','UserController');

Route::resource('/team','TeamController');
Route::post('/team/registerTeam','TeamController@register_team');
Route::get('/login','UserController@login');//check role
Route::get('/logout','UserController@destroy');
Route::get('/loginView','UserController@loginView');

Route::get('/admin/login','AdminController@loginView');

Route::get('/home', 'HomeController@index')->name('user-profile');
Route::get('/home/information', 'HomeController@info')->name('user-information');
Route::get('/home/team', 'HomeController@team')->name('user-team');

Route::group(['prefix' => 'admin', 'middleware' => 'AdminMiddleware'], function() {
	Route::resource('/','AdminController');
	Route::get('/participant', 'AdminController@participant_info');
	Route::get('/participant/verify/{id}','AdminController@verify');
	Route::get('/member/update/{id}','AdminController@participant_update');
	Route::get('/member/delete/{id}','AdminController@participant_delete');
});

Route::post('admin/login','AdminController@login');
Route::get('admin/logout','AdminController@logout');

Route::post('/user/editPassword/{id}','UserController@updatePassword');
Route::get('/user/editviewpassword','UserController@viewEditPassword');
Route::post('/user/editPassword','UserController@editPassword');

Auth::routes();