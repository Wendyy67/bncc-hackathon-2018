<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
	public $table = "informations";
    protected $fillable = [
        'title', 'content',
    ];
}
