<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;
use Auth;
use Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('guest',['except'=>'destroy']);
    }

    public function index()
    {
        return view('index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),[
            'name' => 'required',
            'email' => 'required',
            'password' => 'required|confirmed',
            'gender' => 'required',
            'institution' => 'required',
            'age' => 'required',
            'role' => 'required',
            'phone' => 'required',
            'position' => 'required',
            'status' => 'required',
        ]);

        $count = $request->count;
        $team_id = $request->team_id;
        $name=$request->name;
        $email=$request->email;
        $password = $request->password;
        $gender=$request->gender;
        $institution=$request->institution;
        $position = $request->position;
        $status = $request->status;
        $age = $request->age;
        $role = $request->role;
        $phone = $request->phone; 

        $now = Carbon::now('utc')->toDateTimeString();
        
        for($i = 0; $i < $count; $i++){
            if($i==0){
                $member = new User();
                $member->name = $name[$i];
                $member->gender = $gender[$i];
                $member->email = $email[$i];
                $member->institution = $institution[$i];
                $member->position = $position[$i]; 
                $member->status = $status[$i]; 
                $member->password = bcrypt($password); 
                $member->team_id = $team_id; 
                $member->age = $age[$i];
                $member->phone = $phone[$i];
                $member->role = $role[$i];
                $member->created_at=$now;
                $member->updated_at=$now;         
                $member->save();
                $user = User::latest()->first();
            }
            else{
                $member = new User();
                $member->name = $name[$i];
                $member->gender = $gender[$i];
                $member->email = $email[$i];
                $member->institution = $institution[$i];
                $member->position = $position[$i]; 
                $member->status = $status[$i]; 
                $member->team_id = $team_id;
                $member->age = $age[$i];
                $member->role = $role[$i];
                $member->phone = $phone[$i];   
                $member->created_at=$now;
                $member->updated_at=$now;        
                $member->save();
            }
        }
        Session::flush();
        Auth::loginUsingId($user->id);
        return redirect('/home');

    }

    public function login(Request $request){
        if(!auth()->attempt(request(['email','password']))){
            return back();
        }
        return redirect('/home');
    }   

    public function loginView(){
        return view('login');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(request(),[
                    'name' => 'required',
                    'email' => 'required',
                    'password' => 'required|confirmed',
                    'gender' => 'required',
                    'university' => 'required',
                    'age' => 'required',
                    'role' => 'required',
                    'phone' => 'required',
                    'major' => 'required'
        ]);

        $name=$request->name;
        $gender=$request->gender;
        $email=$request->email;
        $university=$request->university;
        $password = $request->password;
        $count = $request->count;
        $team_id = $request->team_id;
        $age = $request->age;
        $role = $request->role;
        $phone = $request->phone; 
        $major = $request->major;

        $now = Carbon::now('utc')->toDateTimeString();
        
        $user = User::find($id);
        $user->update($request->all());
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        auth()->logout();
        return redirect('/');
    }
}
