<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;
use App\User;
use App\Information;
use Auth;
use Session;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function __contruct(){
    //     $this->middleware('AdminMiddleware');
    // }
//tidak bsa login
    public function login(Request $request){
        if(!auth()->attempt(request(['email','password']))){
            return back();
        }
        return redirect('/admin');        
    }

    public function logout()
    {
        Session::flush();
        auth()->logout();
        return redirect('/');
    }

    public function loginView()
    {
        return view('admin.admin-login');
    }


    public function index()
    {
            Session::put('Admin','true');
            $teams = Team::all();
            return view('admin.admin',compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $informations = Information::all();
        return view('admin.add-announcement',compact('informations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $information = $request->all();
        Information::create($information);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function verify($id){
        
        $teams = Team::findOrFail($id);
        $teams['payment']= TRUE;
        $teams->save();

        return redirect()->back();
    }

    public function participant_info(){
        $participants = User::all();
        return view('admin.admin-participants',compact('participants'));
    }
}
