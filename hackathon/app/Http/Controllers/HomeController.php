<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Information;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $team = auth()->user()->team;
        return view('user-panel.user-profile',compact('team'));
    }

    public function info(){
        $informations = Information::orderBy('created_at','desc')->get();
        $team = auth()->user()->team;
        return view('user-panel.user-information',compact('informations','team'));
    }
}
