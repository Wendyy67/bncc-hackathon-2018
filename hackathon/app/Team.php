<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = [
        'name', 'quantity','user_id',
    ];

    protected $hidden = ['payment'];

    public function users(){
    	return $this->hasMany(User::Class);
    }
}
