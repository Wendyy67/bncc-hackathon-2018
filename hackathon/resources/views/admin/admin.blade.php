<html>
	<head>
    <title>BNCC Hackathon</title>
	<link rel="icon" href="{{asset('resources/assets/img/logo-hackathon.png')}}">s

	    <link rel="stylesheet" media="all" href="{{asset('resources/assets/css/admin.css')}}"/>
	    <link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/font-awesome.min.css')}}">
  	</head>
  	<body>
    	<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
      		<nav id="sidebar">
    			<div id="sidebar-scroll">
        			<div class="sidebar-content">
            			<div class="side-header side-content bg-white-op" style="background-color: white;">
               			<button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                    		<i class="fa fa-times"></i>
                		</button>
		                <a class="h5 text-white" href="{{url('/')}}">
							<img src="{{asset('resources/assets/img/logo-hackathon-small.png')}}" class="img-responsive">
						</a>            
						</div>
	            		<div class="side-content side-content-full">
							<ul class="nav-main">
			                    <li>
			                      	<a href="{{url('admin')}}">
			                        	<i class="fa fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Team Data</span>
									</a>                   
								</li>
			                    <li>
			                      	<a href="{{url('admin/participant')}}">
			                        	<i class="fa fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Participants Data</span>
									</a>                   
								</li>
			                    <li>
			                     	<a href="{{url('admin/create')}}">
			                        	<i class="fa fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Add Announcement</span>
									</a>
								</li>
			                </ul>
	            		</div>
        			</div>
    			</div>
			</nav>

      		<header id="header-navbar" class="content-mini content-mini-full">

		  	<ul class="nav-header pull-right">
		  		<li>
		    		<a rel="nofollow" data-method="delete" href="{{url('/admin/logout')}}">Log out</a>
		  		</li>
			</ul>
			</header>

			<main id="main-container">


		    <div class="content bg-gray-lighter">
			    <div class="row items-push">
			        <div class="col-sm-7">
			            <h1 class="page-heading">
			            	Welcome , Admin BNCC !
			            </h1>
			        </div>
			        <div class="col-sm-5 text-right hidden-xs">
			        </div>
			    </div>
			</div>


			<div class="content">
			  <div class="row">
			    <div class="col-lg-12">
			        <div class="block">
			            <div class="block-content">
              				<table class="table table-responsive table-striped table-borderless table-header-bg">
                  			<thead>
	    						<tr>
							        <th class="text-center">No.</th>	
							        <th class="">Team</th>
							        <th class="">Quantity</th>
							        <th class="">Status</th>
		        					<th class="text-center">Actions</th>
	    						</tr>
							</thead>

							<tbody>
								@foreach($teams as $key => $team)
		                        <tr>
		                            <td class="text-center">{{++$key}}</td>
		                            <td>
		                            	<a data-toggle="modal" href='#{{$team->id}}team'>{{$team->name}}</a>
		                            </td>
									<td>{{$team->quantity}} Members</td>
		                            <td>
		                            	@if($team->payment == true)
		                            		<span style="color: #2ecc71;">Paid</span>
		                            	@else
		                            		<span style="color: #e74c3c">Not Paid</span>
		                            	@endif
		                            </td>
		                            <td class="text-center">
		                                <div class="btn-group">
											<form method="POST" action="{{url('team/'.$team->id)}}">
												{{csrf_field()}}
												<input type="hidden" name="_method" value="DELETE">
			                                    <a class="btn btn-xs btn-default" href="{{url('admin/team/verify/'.$team->id)}}">
			                                    <i class="fa fa-check"></i>
												</a>
			                                    <button class="btn btn-xs btn-default" type="submit">
			                                     <i class="fa fa-times"></i>
												</button> 
											</form>                               
										</div>
		                            </td>
		                        </tr>
		                        @endforeach
							</tbody>
			              </table>
			            </div>
			        </div>
			    </div>
			  </div>		

			<div class="row">
				<div class="col-sm-12">
					<div class="block">
						<div class="block-content">
							<h2>Sponsors</h2>
							<img src="{{asset('resources/assets/img/Sponsors.png')}}" class="img-responsive">
					</div>
				</div>
			</div>

			</main>

			<footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
				<div class="pull-left">
				BNCC Hackathon © <span class="js-year-copy"></span>
				</div>
			</footer>
   		</div>

   		@foreach($teams as $team)
   		<div class="modal fade" id="{{$team->id}}team">
   			<div class="modal-dialog">
   				<div class="modal-content">
   					<div class="modal-header">
   						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   						<h4 class="modal-title">Team {{$team->name}}</h4>
   					</div>
   					<div class="modal-body">

              				<table class="table table-responsive table-striped table-borderless table-header-bg">
                  			<thead>
	    						<tr>
							        <th class="text-center">No.</th>	
							        <th class="">Name</th>
							        <th class="">Role</th>
							        <th class="">Status</th>
							        <th class="">Institution</th>
	    						</tr>
							</thead>

							<tbody>
								@foreach($team->users as $key => $user)
		                        <tr>
		                            <td class="text-center">{{++$key}}</td>
		                            <td>@if(!empty($user->password))
		                            	<span style="color: #2ecc71;">{{$user->name}}</span>
		                            	@else
		                            		{{$user->name}}
		                            	@endif
		                            </td>
									<td>{{$user->role}}</td>
									<td>{{$user->status}}</td>
		                            <td>
		                            	{{$user->institution}}
		                            </td>
		                        </tr>
		                        @endforeach
							</tbody>
			              </table>

   					</div>
   					<div class="modal-footer">
   						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
   					</div>
   				</div>
   			</div>
   		</div>
   		@endforeach

    <script type="text/javascript" src="{{asset('resources/assets/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('resources/assets/js/bootstrap.min.js')}}"></script>
  </body>
</html>
