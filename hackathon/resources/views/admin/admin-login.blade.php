<!DOCTYPE html>
<html>
<head>
        <title>BNCC Hackathon</title>
    <link rel="icon" href="{{asset('resources/assets/img/logo-hackathon.png')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/bootstrap.min.css')}}">  
    <link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/panel.css')}}">
</head>
<body class="body">

<div class="container container-wrapper">
    <div class="row">
        <a href="{{url('/')}}"><img src="{{asset('resources/assets/img/logo-hackathon-small.png')}}" class="img-responsive" style="margin: 0 auto;">
        </a>
        <h3 class="form-title">Admin Login</h3>
        <form class="form-horizontal" method="POST" action="{{url('/admin/login')}}">
            {{ csrf_field() }}

            <div class="input-wrapper {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" class="inputclass" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
            </div>

            <div class="no-bd-top input-wrapper {{ $errors->has('password') ? ' has-error' : '' }}">

                    <input id="password" type="password" class="inputclass" name="password" placeholder="Password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
            </div>

            <div class="container-button">
                    <button type="submit" class="button-class">
                        Login
                    </button>
            </div>
        </form>
    </div>
</div> 

    <script type="text/javascript" src="{{asset('resources/assets/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('resources/assets/js/bootstrap.min.js')}}"></script>
</body>
</html>