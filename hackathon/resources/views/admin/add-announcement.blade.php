<html>
	<head>
    <title>BNCC Hackathon</title>
	<link rel="icon" href="{{asset('resources/assets/img/logo-hackathon.png')}}">

	    <link rel="stylesheet" media="all" href="{{asset('resources/assets/css/admin.css')}}"/>
	    <link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/font-awesome.min.css')}}">
  	</head>
  	<body>
    	<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
      		<nav id="sidebar">
    			<div id="sidebar-scroll">
        			<div class="sidebar-content">
            			<div class="side-header side-content bg-white-op" style="background-color: white;">
               			<button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                    		<i class="fa fa-times"></i>
                		</button>
		                <a class="h5 text-white" href="{{url('/')}}">
							<img src="{{asset('resources/assets/img/logo-hackathon-small.png')}}" class="img-responsive">
						</a>            
						</div>
	            		<div class="side-content side-content-full">
			                <ul class="nav-main">
			                    <li>
			                      	<a href="{{url('admin')}}">
			                        	<i class="fa fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Team Data</span>
									</a>                   
								</li>
			                    <li>
			                      	<a href="{{url('admin/participant')}}">
			                        	<i class="fa fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Participants Data</span>
									</a>                   
								</li>
			                    <li>
			                     	<a href="{{url('admin/create')}}">
			                        	<i class="fa fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Add Announcement</span>
									</a>
								</li>
			                </ul>
	            		</div>
        			</div>
    			</div>
			</nav>

      		<header id="header-navbar" class="content-mini content-mini-full">

		  	<ul class="nav-header pull-right">
		  		<li>
		    		<a rel="nofollow" data-method="delete" href="{{url('/admin/logout')}}">Log out</a>
		  		</li>
			</ul>
			</header>

			<main id="main-container">


		    <div class="content bg-gray-lighter">
			    <div class="row items-push">
			        <div class="col-sm-7">
			            <h1 class="page-heading">
			            	Welcome , Admin BNCC !
			            </h1>
			        </div>
			        <div class="col-sm-5 text-right hidden-xs">
			        </div>
			    </div>
			</div>

			<div class="content">
			  <div class="row">
			    <div class="col-lg-12">
				        <div class="block">
				            <div class="block-content" style="padding: 20px 20px;">
				            	<h2>Announcement Published</h2>
								<div id="accordion" style="margin-top: 20px;border-radius: 15px;cursor: pointer; ">
									@foreach($informations as $information)
									  <div class="card">
									    <div class="card-header" style="background-color:#5c90d2;padding: 5px;color: #f1f1f1;"id="heading{{$information->id}}">
									      <h3 class="mb-0" data-toggle="collapse" data-target="#info{{$information->id}}" aria-expanded="true" aria-controls="info{{$information->id}}">
									          {{$information->title}}
									      </h3>
									    </div>

									    <div id="info{{$information->id}}" class="collapse" aria-labelledby="heading{{$information->id}}" data-parent="#accordion">
									      <div class="card-body" style="margin-top: 10px;">
									          {{$information->content}}
									      </div>
									    </div>
									  </div>
								  @endforeach
								</div>
				            </div>
				        </div>
				    </div>
				</div>
			</div>

			<div class="content">
			  <div class="row">
			    <div class="col-lg-12">
			        <div class="block">
			            <div class="block-content">
			            	<form action="{{url('admin')}}" method="POST" role="form">
			            		{{csrf_field()}}
			            		<legend>Add Announcement</legend>
			            		
			            		<div class="form-group">
			            			<label for="">Input Title</label>
			            			<input type="text" class="form-control" id="" placeholder="Input Title" name="title">
			            		</div>

			            		<div class="form-group">
			            			<label for="">Input Content</label>
			            			<textarea name="content" class="form-control" placeholder="Input Content"></textarea>
			            		</div>	
			            	
			            		<button type="submit" class="btn btn-primary">Submit</button>
			            	</form>
			            </div>
			        </div>
			    </div>
			  </div>		

			<div class="row">
				<div class="col-sm-12">
					<div class="block">
						<div class="block-content">
							<h2>Sponsors</h2>
							<img src="{{asset('resources/assets/img/Sponsors.png')}}" class="img-responsive">
					</div>
				</div>
			</div>

			</main>

			<footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
				<div class="pull-left">
				BNCC Hackathon © <span class="js-year-copy"></span>
				</div>
			</footer>
   		</div>

    <script type="text/javascript" src="{{asset('resources/assets/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('resources/assets/js/bootstrap.min.js')}}"></script>
  </body>
</html>
