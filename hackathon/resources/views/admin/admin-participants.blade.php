<html>
	<head>
    <title>BNCC Hackathon</title>
		<link rel="icon" href="{{asset('resources/assets/img/logo-hackathon.png')}}">
	    <link rel="stylesheet" media="all" href="{{asset('resources/assets/css/admin.css')}}"/>
	    <link rel="stylesheet" media="all" href="{{asset('resources/assets/css/panel.css')}}"/>
	    <link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/font-awesome.min.css')}}">
  	</head>
  	<body>
    	<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
      		<nav id="sidebar">
    			<div id="sidebar-scroll">
        			<div class="sidebar-content">
            			<div class="side-header side-content bg-white-op" style="background-color: white;">
               			<button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                    		<i class="fa fa-times"></i>
                		</button>
		                <a class="h5 text-white" href="{{url('/')}}">
							<img src="{{asset('resources/assets/img/logo-hackathon-small.png')}}" class="img-responsive">
						</a>            
						</div>
	            		<div class="side-content side-content-full">
			                			                <ul class="nav-main">
			                    <li>
			                      	<a href="{{url('admin')}}">
			                        	<i class="fa fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Team Data</span>
									</a>                   
								</li>
			                    <li>
			                      	<a href="{{url('admin/participant')}}">
			                        	<i class="fa fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Participants Data</span>
									</a>                   
								</li>
			                    <li>
			                     	<a href="{{url('admin/create')}}">
			                        	<i class="fa fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Add Announcement</span>
									</a>
								</li>
			                </ul>
	            		</div>
        			</div>
    			</div>
			</nav>

      		<header id="header-navbar" class="content-mini content-mini-full">

		  	<ul class="nav-header pull-right">
		  		<li>
		    		<a rel="nofollow" data-method="delete" href="{{url('/admin/logout')}}">Log out</a>
		  		</li>
			</ul>
			</header>

			<main id="main-container">


		    <div class="content bg-gray-lighter">
			    <div class="row items-push">
			        <div class="col-sm-7">
			            <h1 class="page-heading">
			            	Welcome , Admin BNCC !
			            </h1>
			        </div>
			        <div class="col-sm-5 text-right hidden-xs">
			        </div>
			    </div>
			</div>


			<div class="content">
			  <div class="row">
			    <div class="col-lg-12">
			        <div class="block">
			            <div class="block-content">
              				<table class="table table-responsive table-striped table-borderless table-header-bg">
                  			<thead>
	    						<tr>
							        <th class="text-center">No.</th>	
							        <th class="">Name</th>
							        <th class="">Email</th>
							        <th class="">Age</th>
							        <th class="">Team</th>
							        <th class="">Phone</th>
							        <th class="">Status</th>
							        <th class="">Institution</th>
							        <th class="">Position</th>
		        					<th class="text-center">Action</th>
	    						</tr>
							</thead>

							<tbody>
								@foreach($participants as $key => $user)
								@if($user->id==1)

								@else
		                        <tr>
		                            <td class="text-center">{{++$key}}</td>
		                            <td>
		                            	<a data-toggle="modal" href='#{{$user->id}}user'>{{$user->name}}
		                            	</a>
		                            </td>
		                            <td>
		                            	{{$user->email}}
		                            </td>
		                            <td>
		                            	{{$user->age}}
		                            </td>
									<td>{{\App\Team::find($user->team_id)['name']}}</td>
		                            <td>
		                            	{{$user->phone}}
		                            </td>
		                            <td>
		                            	{{$user->status}}
		                            </td>
		                            <td>
		                            	{{$user->institution}}
		                            </td>
		                            <td>
		                            	{{$user->position}}
		                            </td>
		                            <td class="text-center">
		                                <div class="btn-group">
		                                    <a class="btn btn-xs btn-default" data-toggle="modal" href='#{{$user->id}}user'>
		                                    <i class="fa fa-pencil"></i>
											</a>
		                                    <a class="btn btn-xs btn-default" href="">
		                                     <i class="fa fa-times"></i>
											</a>                                
										</div>
		                            </td>
		                        </tr>
		                        @endif
		                        @endforeach
							</tbody>
			              </table>
			            </div>
			        </div>
			    </div>
			  </div>		

			<div class="row">
				<div class="col-sm-12">
					<div class="block">
						<div class="block-content">
							<h2>Sponsors</h2>
							<img src="{{asset('resources/assets/img/Sponsors.png')}}" class="img-responsive">
					</div>
				</div>
			</div>

			</main>

			<footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
				<div class="pull-left">
				BNCC Hackathon © <span class="js-year-copy"></span>
				</div>
			</footer>
   		</div>

   		@foreach($participants as $user)
   		<div class="modal fade" id="{{$user->id}}user">
   			<div class="modal-dialog">
   				<div class="modal-content">
   					<div class="modal-header">
   						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
   						<h4 class="modal-title">Participant {{$user->name}}</h4>
   					</div>
   					<div class="modal-body">

						<form action="{{url('member/'.$user->id)}}" method="POST" role="form">
						{{csrf_field()}}
						<input type="hidden" name="_method" value="PATCH">
						<div class="input-wrapper">
							<input type="text" name="name" class="inputclass" id="" placeholder="Input Name" value="{{$user->name}}" required>
						</div>
						
						<div class="no-bd-top input-wrapper">
							<input type="email" name="email" class="inputclass" id="" placeholder="Input Email" value="{{$user->email}}" required>
						</div>

						<div class="no-bd-top input-wrapper">
							<select name="gender[]" class="inputclass" required="required">
			  					<option class="inputclass" value="" selected disabled hidden>Input Gender</option>
								<option class="inputclass" @if($user->gender == 'Male') selected @endif value="Male">Male </option>
								<option class="inputclass" @if($user->gender == 'Female') selected @endif value="Female">Female</option>
							</select>
						</div>

						<div class="no-bd-top input-wrapper">
							<input type="number" name="age" class="inputclass" id="" placeholder="Input Age" value="{{$user->age}}" required>
						</div>

						<div class="no-bd-top input-wrapper">
							<select name="role" id="inputRole[]" class="inputclass" required="required">
			  					<option class="inputclass" value="" selected disabled hidden>Input Hackathon Role</option>
								<option class="inputclass" @if($user->role == 'Hustler') selected @endif  value="Hustler">Huster (Marketing) </option>
								<option class="inputclass" @if($user->role == 'Hipster') selected @endif disabled=" " value="Hipster">Hipster (Designer) </option>
								<option class="inputclass" @if($user->role == 'Developer') selected @endif  value="Hacker">Hacker (Developer) </option>
							</select>
						</div>

						<div class="no-bd-top input-wrapper">
							<input type="text" name="university" class="inputclass id="" placeholder="Input University" value="{{$user->university}}" required>
						</div>

						<div class="no-bd-top input-wrapper">
							<input type="text" name="major" class="inputclass" id="" placeholder="Input Major" value="{{$user->major}}" required>
						</div>

						<div class="container-button-regis col-sm-4 col-sm-offset-4" style="margin-top: 20px;">
						<button type="submit" class="button-class">Submit</button>
						</div>

						</form>

   					</div>
   					<div class="modal-footer">
   						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
   					</div>
   				</div>
   			</div>
   		</div>
   		@endforeach

    <script type="text/javascript" src="{{asset('resources/assets/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('resources/assets/js/bootstrap.min.js')}}"></script>
  </body>
</html>


