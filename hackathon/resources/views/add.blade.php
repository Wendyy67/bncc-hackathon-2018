<!DOCTYPE html>
<html>
<head>
    <title>BNCC Hackathon</title>
    <link rel="icon" href="{{asset('resources/assets/img/logo-hackathon.png')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/bootstrap.min.css')}}">	
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/panel.css')}}">
</head>
<body class="body">

	<div class="container-fluid">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
		<form action="{{url('member')}}" method="POST" role="form">
			{{csrf_field()}}
			
			@if(session('quantity')==1)

			<input type="hidden" name="team_id" value="{{session('id')}}">
			<input type="hidden" name="count" value="{{session('quantity')}}">	
			<div class="container-wrapper-fluid">
			<h3 class="form-title">Register <b>{{session('name')}}</b>Team Members</h3>
			<div class="col-sm-4" style="margin-bottom: 30px;">

			<h3>Team's Leader</h3>

			<div class="input-wrapper">
				<input type="text" name="name[]" class="inputclass" id="" placeholder="Input Name" required>
			</div>
			
			<div class="no-bd-top input-wrapper">
				<input type="email" name="email[]" class="inputclass" id="" placeholder="Input Email" required>
			</div>

				<div class="no-bd-top input-wrapper">
					<input type="password" name="password" class="inputclass" id="" placeholder="Input Password" required>
				</div>

				<div class="no-bd-top input-wrapper">
					<input type="password" name="password_confirmation" class="inputclass" id="password_confirmation" placeholder="Input Password Confirmation" required>
				</div>


			<div class="no-bd-top input-wrapper">
				<select name="gender[]" class="inputclass" required="required">
  					<option class="inputclass" value="" selected disabled hidden>Input Gender</option>
					<option class="inputclass" value="Male">Male</option>
					<option class="inputclass" value="Female">Female</option>
				</select>
			</div>

				<div class="no-bd-top input-wrapper">
					<input type="number" name="age[]" class="inputclass" id="" placeholder="Input Age" required>
				</div>

				<div class="no-bd-top input-wrapper">
					<input type="text" name="phone[]" class="inputclass" id="" placeholder="Input Phone Number [08]" pattern="[08][0-9]{9,}" required>
				</div>

				<div class="no-bd-top input-wrapper">
					<select name="role[]" id="inputRole[]" class="inputclass" required="required">
	  					<option class="inputclass" value="" selected disabled hidden>Input Hackathon Role</option>
						<option class="inputclass" value="Hustler">Huster (Marketing) </option>
						<option class="inputclass" value="Hipster">Hipster (Designer) </option>
						<option class="inputclass" value="Hacker">Hacker (Developer) </option>
					</select>
				</div>

				<div class="no-bd-top input-wrapper" id="status-wrapper">
					<select id="status" name="status[]" class="inputclass" required="required">
	  					<option class="inputclass" value="" selected disabled hidden>Input Current Status</option>
						<option class="inputclass" value="Unemployed">Unemployed</option>
						<option class="inputclass" value="Employed">Employed</option>
						<option class="inputclass" value="Student">Student</option>
					</select>
				</div>

			</div>
			<div class="container-button-regis col-sm-4 col-sm-offset-4" style="margin-top: 20px;">
			<button type="submit" class="button-class">Submit</button>
			</div>

			@elseif(session('quantity')>1)

			<input type="hidden" name="team_id" value="{{session('id')}}">
			<input type="hidden" name="count" value="{{session('quantity')}}">	
			<div class="container-wrapper-fluid">
			<h3 class="form-title">Register Team Members</h3>
				@for($i=0;$i<session('quantity');$i++)
				<div class="col-sm-4" style="margin-bottom: 30px;">

					@if($i==1)
						<h3>Team's Leader</h3>
					@elseif($i==0)
						<h3>2nd Team Member</h3>
					@elseif($i==2)
						<h3>3rd Team Member</h3>	
					@endif

				<div class="input-wrapper">
					<input type="text" name="name[]" class="inputclass" id="" placeholder="Input Name" required>
				</div>
				
				<div class="no-bd-top input-wrapper">
					<input type="email" name="email[]" class="inputclass" id="" placeholder="Input Email" required>
				</div>

					@if($i==1)
						<div class="no-bd-top input-wrapper">
							<input type="password" name="password" class="inputclass" id="" placeholder="Input Password" required>
						</div>

						<div class="no-bd-top input-wrapper">
							<input type="password" name="password_confirmation" class="inputclass" id="password_confirmation" placeholder="Input Password Confirmation" required>
						</div>
					@endif

				<div class="no-bd-top input-wrapper">
					<select name="gender[]" class="inputclass" required="required">
	  					<option class="inputclass" value="" selected disabled hidden>Input Gender</option>
						<option class="inputclass" value="Male">Male </option>
						<option class="inputclass" value="Female">Female</option>
					</select>
				</div>

				<div class="no-bd-top input-wrapper">
					<input type="number" name="age[]" class="inputclass" id="" placeholder="Input Age" required>
				</div>

				<div class="no-bd-top input-wrapper">
					<input type="text" name="phone[]" class="inputclass" id="" placeholder="Input Phone Number [08]" pattern="[08][0-9]{9,}" required>
				</div>

				<div class="no-bd-top input-wrapper">
					<select name="role[]" id="inputRole[]" class="inputclass" required="required">
	  					<option class="inputclass" value="" selected disabled hidden>Input Hackathon Role</option>
						<option class="inputclass" value="Hustler">Huster (Marketing) </option>
						<option class="inputclass" value="Hipster">Hipster (Designer) </option>
						<option class="inputclass" value="Hacker">Hacker (Developer) </option>
					</select>
				</div>

				<div class="no-bd-top input-wrapper" id="status-wrapper">
					<select id="status" name="status[]" class="inputclass" required="required">
	  					<option class="inputclass" value="" selected disabled hidden>Input Current Status</option>
						<option class="inputclass" value="Unemployed">Unemployed</option>
						<option class="inputclass" value="Employed">Employed</option>
						<option class="inputclass" value="Student">Student</option>
					</select>
				</div>

				</div>
				@endfor
			<div class="container-button-regis col-sm-4 col-sm-offset-4" style="margin-top: 20px;">
			<button type="submit" class="button-class">Submit</button>
			</div>
			@endif

			</div>
		</form>
	</div>

	<script type="text/javascript" src="{{asset('resources/assets/js/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('resources/assets/js/bootstrap.min.js')}}"></script>

	<script type="text/javascript">
   (function($) {
		$('#status').change(function(event){
			event.preventDefault();
			var status = $('#status').val();
			var status_wrapper = $('#status-wrapper');
			var employed_info1 = $('#employed-info1');
			var employed_info2 = $('#employed-info2');
			var student_info1 = $('#student-info1');
			var student_info2 = $('#student-info2');
			var university = $('#university');
			var major = $('#major');
			var institution = $('#institution');
			var position = $('#position');
			alert(status);
			if(status=='Employed'){
				if(!university.length || !major.length || !position.length || !institution.length){
					employed_info1.remove();
					employed_info2.remove();
					student_info1.remove();
					student_info2.remove();
					status_wrapper.after("<div class='no-bd-top input-wrapper' id='employed-info1'><input type='text' name='institution[]' class='inputclass' id='institution' placeholder='Input Institution' required></div><div class='no-bd-top input-wrapper' id='employed-info2'><input type='text' name='position[]' class='inputclass' id='position' placeholder='Input Position' required></div>");
				}
			}else if(status=='Student'){
				if(!university.length || !major.length || !position.length || !institution.length){
					employed_info1.remove();
					employed_info2.remove();
					student_info1.remove();
					student_info2.remove();
					status_wrapper.after("<div class='no-bd-top input-wrapper' id='student-info1'><input type='text' name='institution[]' class='inputclass' id='university' placeholder='Input University' required></div><div class='no-bd-top input-wrapper' id='student-info2'><input type='text' name='position[]' class='inputclass' id='major' placeholder='Input Major' required></div>");
				}
			}else if(status=='Unemployed'){
					employed_info1.remove();
					employed_info2.remove();
					student_info1.remove();
					student_info2.remove();
			}
		});
	})(jQuery);
	</script>

</body>
</html>