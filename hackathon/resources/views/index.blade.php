<!DOCTYPE html>
<html>
<head>
	<title>BNCC Hackathon</title>
	<link rel="icon" href="{{asset('resources/assets/img/logo-hackathon.png')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/style.css')}}">
</head>
<body data-spy="scroll" data-target="#myScrollspy" data-offset="20">	

	<nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="myScrollspy">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><img src="{{asset('resources/assets/img/logo-hackathon-Small.png')}}" class="img-responsive" width="200" height="auto"></a>
			</div>

			<div class="collapse navbar-collapse navbar-ex1-collapse">	
				<ul class="nav navbar-nav navbar-right">
					<li class="home-nav"><a href="#home-nav">Home</a></li>
					<li class="about-nav"><a href="#about-nav">About</a></li>
					<li class="faq-nav"><a href="#faq-nav">FAQ</a></li>
					<li class="sponsor-nav"><a href="#sponsor-nav">Sponsors</a></li>
					<li class="contact-nav"><a href="#contact-nav">Contact</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div>
	</nav>

	<div class="container-fluid header" id="home-nav">
		<div class="overlay" >
			<img src="{{asset('resources/assets/img/logo-hackathon.png')}}" class="header-logo img-responsive" >
			<div class="container overlay-text" >
				<h1 class="header-title">Road to BNCC Techno Career</h1>
				<h3 class="header-description">Code Your Future</h3>
				<div class="container-button">
				<a href="{{url('team')}}"><button class="btn btn-custom btn-primary btn-lg col-sm-6" style="float: right;;">Register</button></a>
				<a href="@if(session('admin')=='true'){{url('admin')}}@else{{url('loginView')}}@endif">
					@if(Auth::check())
					<button class="btn btn-custom btn-primary btn-lg col-sm-6" style="float: left;">Admin</button>
					@else
					<button class="btn btn-custom btn-primary btn-lg col-sm-6" style="float: left;">Login
					</button>	
					@endif
				</a>
			</div>
		</div>	
	</div>

	<div class="container-fluid about" id="about-nav">
		<h2 class="text-center title-about">BNCC Hackathon</h2>
		<h3 class="text-center small-title-about">Code Your Future </h3>
		<div class="row row-about">
			<div class="col-sm-12 about-description">
				BNCC Hackathon 2018 merupakan serangkaian acara kompetisi di bidang IT yang diselenggarakan oleh Bina Nusantara Computer Club (BNCC). Pada tahun ini BNCC Hackathon hadir dengan mengangkat tema “Code Your Future”. BNCC Hackathon ingin mengajak seluruh masyarakat di Indonesia untuk bereksplorasi dan bertindak secara kreatif dalam memberikan solusi terhadap masalah-masalah yang sedang dihadapi pada era modern ini melalui inovasi yang berbasis teknologi digital. BNCC Hackathon juga ingin menjadi event yang mampu memberikan wadah dan sarana bagi masyarakat Indonesia khususnya yang memiliki keahlian dalam bidang pemograman untuk terjun langsung menghadapi masalah-masalah yang dihadapi di kehidupan nyata sekarang ini.
				Serangkaian kompetisi yang dilombakan di acara BNCC Hackathon 2018 meliputi: xxx
			</div>
			<div class="col-sm-6">
				<img style="margin: 50px auto;" src="{{asset('resources/assets/img/Logo-Placeholder.png')}}" class="img-responsive" width="160" height="auto">
				<h3 class="text-center Logo-about-description">Binus University</h3>
			</div>
			<div class="col-sm-6">
				<img style="margin: 50px auto;" src="{{asset('resources/assets/img/Logo-Calendar.png')}}" class="img-responsive" width="160" height="auto">
				<h3 class="text-center Logo-about-description">13-14 April 2018</h3>
			</div>
			<div class="col-sm-12">
				<h3 class="text-left">Deadline
				<span class="text-left-description">Registration 10 April 2018</span>
				</h3>
			</div>
		</div>
	</div>

	<div class="container-fluid faq" id="faq-nav">
		<h2 class="text-center faq-title">Frequently Asked Question</h2>
		<div class="row" style="margin-top: 50px;">
			<div class="col-sm-offset-1 col-sm-5">
				<a data-toggle="collapse" href="#collapse1" style="text-decoration: none;" class="rotate-trigger">
				  <div class="panel-group">
				    <div class="panel panel-default">
				      <div class="panel-heading">
				        <h4 class="panel-title" style="font-size: 1.5em;">
				          Questions
				          <img class="rotate" src="{{asset('resources/assets/img/arrow.png')}}" style="float:right;height: 20px;">
				        </h4>
				      </div>
				      <div id="collapse1" class="panel-collapse collapse">
				        <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
				      </div>
				    </div>
				  </div>
				</a>
				<a data-toggle="collapse" href="#collapse2" style="text-decoration: none;" class="rotate-trigger">
				  <div class="panel-group">
				    <div class="panel panel-default">
				      <div class="panel-heading">
				        <h4 class="panel-title" style="font-size: 1.5em;">
				          Questions
				          <img class="arrow rotate" src="{{asset('resources/assets/img/arrow.png')}}">
				        </h4>
				      </div>
				      <div id="collapse2" class="panel-collapse collapse">
				        <div class="panel-body">Description</div>
				      </div>
				    </div>
				  </div>
				</a>
				<a data-toggle="collapse" href="#collapse3" style="text-decoration: none;" class="rotate-trigger">
				  <div class="panel-group">
				    <div class="panel panel-default">
				      <div class="panel-heading">
				        <h4 class="panel-title" style="font-size: 1.5em;">
				          Questions
				          <img class="arrow rotate" src="{{asset('resources/assets/img/arrow.png')}}">
				        </h4>
				      </div>
				      <div id="collapse3" class="panel-collapse collapse">
				        <div class="panel-body">Description</div>
				      </div>
				    </div>
				  </div>
				</a>
				<a data-toggle="collapse" href="#collapse4" style="text-decoration: none;" class="rotate-trigger">
				  <div class="panel-group">
				    <div class="panel panel-default">
				      <div class="panel-heading">
				        <h4 class="panel-title" style="font-size: 1.5em;">
				          Questions
				          <img class="arrow rotate" src="{{asset('resources/assets/img/arrow.png')}}">
				        </h4>
				      </div>
				      <div id="collapse4" class="panel-collapse collapse">
				        <div class="panel-body">Description</div>
				      </div>
				    </div>
				  </div>
				</a>
			</div>
			<div class="col-sm-5">
				<a data-toggle="collapse" href="#collapse5" style="text-decoration: none;" class="rotate-trigger">
				  <div class="panel-group">
				    <div class="panel panel-default">
				      <div class="panel-heading">
				        <h4 class="panel-title" style="font-size: 1.5em;">
				          Questions
				          <img class="arrow rotate" src="{{asset('resources/assets/img/arrow.png')}}">
				        </h4>
				      </div>
				      <div id="collapse5" class="panel-collapse collapse">
				        <div class="panel-body">Description</div>
				      </div>
				    </div>
				  </div>
				</a>
				<a data-toggle="collapse" href="#collapse6" style="text-decoration: none;" class="rotate-trigger">
				  <div class="panel-group">
				    <div class="panel panel-default">
				      <div class="panel-heading">
				        <h4 class="panel-title" style="font-size: 1.5em;">
				          Questions
				          <img class="arrow rotate" src="{{asset('resources/assets/img/arrow.png')}}">
				        </h4>
				      </div>
				      <div id="collapse6" class="panel-collapse collapse">
				        <div class="panel-body">Description</div>
				      </div>
				    </div>
				  </div>
				</a>
				<a data-toggle="collapse" href="#collapse7" style="text-decoration: none;" class="rotate-trigger">
				  <div class="panel-group">
				    <div class="panel panel-default">
				      <div class="panel-heading">
				        <h4 class="panel-title" style="font-size: 1.5em;">
				          Questions
				          <img class="arrow rotate" src="{{asset('resources/assets/img/arrow.png')}}">
				        </h4>
				      </div>
				      <div id="collapse7" class="panel-collapse collapse">
				        <div class="panel-body">Description</div>
				      </div>
				    </div>
				  </div>
				</a>
				<a data-toggle="collapse" href="#collapse8" style="text-decoration: none;" class="rotate-trigger">
				  <div class="panel-group">
				    <div class="panel panel-default">
				      <div class="panel-heading">
				        <h4 class="panel-title" style="font-size: 1.5em;">
				          Questions
				          <img class="arrow rotate" src="{{asset('resources/assets/img/arrow.png')}}">
				        </h4>
				      </div>
				      <div id="collapse8" class="panel-collapse collapse">
				        <div class="panel-body">Description</div>
				      </div>
				    </div>
				  </div>
				</a>
			</div>
		</div>
	</div>

	<div class="container-fluid sponsors" id="sponsor-nav">
		<h2 class="text-center title-about color-black">Platinum Sponsors</h2>
		<div class="row" style="padding: 0 50px;">
			<div class="col-sm-12 img-wrapper">
				<img src="{{asset('resources/assets/img/logo-google.png')}}" class="img-responsive" width="200" style="margin: 0 auto;">
			</div>
		</div>
		<h2 class="text-center title-about color-black">Gold Sponsors</h2>
		<div class="row" style="margin-top: 50px;">
			<div class="col-sm-offset-1 col-sm-5 img-wrapper">
				<img src="{{asset('resources/assets/img/logo-google.png')}}" class="img-responsive" width="200" style="margin: 0 auto;">
			</div>
			<div class="col-sm-5 img-wrapper">
				<img src="{{asset('resources/assets/img/logo-google.png')}}" class="img-responsive" width="200" style="margin: 0 auto;">
			</div>
		</div>
		<h2 class="text-center title-about color-black">Silver Sponsors</h2>
		<div class="row" style="margin-top: 50px;">
			<div class="col-sm-offset-1 col-sm-5 img-wrapper">
				<img src="{{asset('resources/assets/img/logo-google.png')}}" class="img-responsive" width="200" style="margin: 0 auto;">
			</div>
			<div class="col-sm-5 img-wrapper">
				<img src="{{asset('resources/assets/img/logo-google.png')}}" class="img-responsive" width="200" style="margin: 0 auto;">
			</div>
		</div>
	</div>

	<div class="container-fluid contact" id="contact-nav">
		<div class="row">
			<h2 class="text-center title-about color-white">Contact Us</h2>
			<div class="col-sm-12">
				<img src="{{asset('resources/assets/img/Logo-Hackathon-Shadow.png')}}" class="img-responsive" style="max-width: 50%;margin: 0 auto;">
			</div>
				<div class="col-sm-3 text-center container-divider">
					<img src="{{asset('resources/assets/img/Logo-mail.png')}}" width="80"> 
					<h4>Dummy@Dummy.com</h4>
				</div>
				<div class="col-sm-3 text-center container-divider">
					<img src="{{asset('resources/assets/img/Logo-instagram.png')}}" width="70"> 
					<h4>BNCCBinus</h4>
				</div>
				<div class="col-sm-3 text-center container-divider">
					<img src="{{asset('resources/assets/img/Logo-facebook.png')}}" width="70"> 
					<h4>BNCC</h4>
				</div>
				<div class="col-sm-3 text-center container-divider">
					<img src="{{asset('resources/assets/img/Logo-phone.png')}}" width="70"> 
					<h4>085263654009 (Wendy)</h4>
				</div>
		</div>
	</div>


	<div class="container-fluid organizer">
		<h2 class="text-center title-about color-black">About the Organizer</h2>
		<a href="http://bncc.net"><img src="{{asset('resources/assets/img/Logo-BNCC.png')}}" class="img-responsive organizer-logo" style="margin: 40px auto;"></a>
		<div class="row row-about">
			<div class="col-sm-12 about-description">
				Bina Nusantara Computer Club (BNCC) merupakan satu-satunya unit kegiatan mahasiswa yang berbasiskan komputer di Universitas Bina Nusantara yang telah berdiri sejak tahun 1989. Sebagai organisasi non-profit yang telah berdiri selama 28 tahun, BNCC memiliki 3 produk yaitu software house <a href="http://favesolution.com">FAVESOLUTION</a>, online media <a href="http://filemagz.com">FileMagz.com</a> dan kelas pembelajaran Learning and Training. Selain itu BNCC juga mempunyai External Event Organizer, Overclocking Community, dan Technopreneur Community.
			</div>
		</div>
	</div>

	<div class="container-fluid footer text-center">
		Copyright of BNCC 2018
	</div>

	<script type="text/javascript" src="{{asset('resources/assets/js/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('resources/assets/js/bootstrap.min.js')}}"></script>
	<script type="text/javascript">
		$(".navbar-brand,.home-nav").click(function (event){
			event.preventDefault();
	        $('html, body').animate({
	            scrollTop: 0
	        }, 800,'swing');
	     });
		$(".about-nav").click(function (event){
			event.preventDefault();
	        $('html, body').animate({
	            scrollTop: 620
	        }, 800,'swing');
	     });
		$(".faq-nav").click(function (event){
			event.preventDefault();
	        $('html, body').animate({
	            scrollTop: 1300
	        }, 800,'swing');
	     });
		$(".sponsor-nav").click(function (event){
			event.preventDefault();
	        $('html, body').animate({
	            scrollTop: 1850
	        }, 800,'swing');
	     });
		$(".contact-nav").click(function (event){
			event.preventDefault();
	        $('html, body').animate({
	            scrollTop: 2450
	        }, 800,'swing');
	     });
	</script>
	<script type="text/javascript">
		@if(session('Login-Alert')){
			alert("{{session('Login-Alert')}}")
		}
		@endif
	</script>
</body>
</html>

