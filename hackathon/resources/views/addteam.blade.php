<!DOCTYPE html>
<html>
<head>
    <title>BNCC Hackathon</title>
	<link rel="icon" href="{{asset('resources/assets/img/logo-hackathon.png')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/bootstrap.min.css')}}">  
    <link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/panel.css')}}">
</head>
<body class="body">

<div class="container container-wrapper">
    <div class="row">
        <a href="{{url('/')}}"><img src="{{asset('resources/assets/img/logo-hackathon-small.png')}}" class="img-responsive" style="margin: 0 auto;">
        </a>
        <h3 class="form-title">Register Team</h3>
        <form class="form-horizontal" method="POST" action="{{url('team')}}">
            {{csrf_field()}}
            <div class="input-wrapper">
                    <input type="text" class="inputclass" name="name"  placeholder="Team Name" required autofocus>
            </div>

            <div class="no-bd-top input-wrapper">

                    <input type="number" class="inputclass" name="quantity" placeholder="Team Quantity" max="3" required>

            </div>

            <div class="container-button">
                    <button type="submit" class="button-class">
                        Register Team
                    </button>
            </div>
        </form>
    </div>
</div> 

    <script type="text/javascript" src="{{asset('resources/assets/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('resources/assets/js/bootstrap.min.js')}}"></script>
</body>
</html>