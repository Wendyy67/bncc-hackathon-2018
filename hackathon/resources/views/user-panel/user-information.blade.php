<html>
	<head>
    <title>BNCC Hackathon</title>
	<link rel="icon" href="{{asset('resources/assets/img/logo-hackathon.png')}}">
	    <link rel="stylesheet" media="all" href="{{asset('resources/assets/css/admin.css')}}"/>
	    <link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/font-awesome.min.css')}}">
  	</head>
  	<body>
    	<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
      		<nav id="sidebar">
    			<div id="sidebar-scroll">
        			<div class="sidebar-content">
            			<div class="side-header side-content bg-white-op" style="background-color: white;">
               			<button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                    		<i class="fa fa-times"></i>
                		</button>
		                <a class="h5 text-white" href="{{url('/')}}">
							<img src="{{asset('resources/assets/img/logo-hackathon-small.png')}}" class="img-responsive">
						</a>            
						</div>
	            		<div class="side-content side-content-full">
			                <ul class="nav-main">
			                    <li>
			                      	<a href="{{route('user-information')}}">
			                        	<i class="fa fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Information</span>
									</a>                   
								</li>
			                    <li>
			                     	<a href="{{route('user-profile')}}">
			                        	<i class="fa fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Profile</span>
									</a>
								</li>
			                </ul>
	            		</div>
        			</div>
    			</div>
			</nav>

      		<header id="header-navbar" class="content-mini content-mini-full">

		  	<ul class="nav-header pull-right">
		  		<li>
		    		<a rel="nofollow" data-method="delete" href="{{url('/logout')}}">Log out</a>
		  		</li>
			</ul>
			</header>

			<main id="main-container">

				@if($team->payment == false)
				
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<strong>Still not verified</strong><br><br>Please Complete Payment of {{$team->quantity}} person with the payment of Rp. 
							<b>{{number_format($team->quantity*50000)}}</b>
							to Bank Account BCA (127983712389) ASAP !  
					</div>
				@else

				@endif

		    <div class="content bg-gray-lighter">
			    <div class="row items-push">
			        <div class="col-sm-7">
			            <h1 class="page-heading">
			            	Welcome , Team {{$team->name}} !
			            </h1>
			        </div>
			        <div class="col-sm-5 text-right hidden-xs">
			        	Status : 
			        	@if($team->payment == true)
			        	<span style="color: #2ecc71;">Verified</span>
			        	@else
			        	<span style="color: #e74c3c">Waiting for Payment</span>
			        	@endif
			        </div>
			    </div>
			</div>


			<div class="content">

			@foreach($informations as $info)
			  <div class="row">
			    <div class="col-lg-12">
			        <div class="block ">
			            <div class="block-content">
			            <h2 style="margin-bottom: 20px;">{{$info->title}}&nbsp;&nbsp;&nbsp;<small style="color: #e74c3c;">New</small></h2> 
						<p style="text-align: justify;">{{$info->content}}
						</p>
			            </div>
			        </div>
			    </div>
			  </div>
			  @endforeach	

			  <div class="row">
			    <div class="col-lg-12">
			        <div class="block">
			            <div class="block-content">
			            <h2 style="margin-bottom: 20px;">About Hackathon</h2> 
						<p style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
						</p>
			            </div>
			        </div>
			    </div>
			  </div>

				<div class="row">
				    <div class="col-lg-12">
				        <div class="block">
				            <div class="block-content">
				            <h2 style="margin-bottom: 20px;" class="text-center">About Organizer</h2> 
				            <a href="http://bncc.net"><img src="{{asset('resources/assets/img/logo-BNCC.png')}}" class="img-responsive" width="50%" style="margin: 0 auto;"></a>
							<p style="text-align: justify;margin-top: 20px;">
								Bina Nusantara Computer Club (BNCC) merupakan satu-satunya unit kegiatan mahasiswa yang berbasiskan komputer di Universitas Bina Nusantara yang telah berdiri sejak tahun 1989. Sebagai organisasi non-profit yang telah berdiri selama 28 tahun, BNCC memiliki 3 produk yaitu software house FAVESOLUTION, online media FileMagz.com dan kelas pembelajaran Learning and Training. Selain itu BNCC juga mempunyai External Event Organizer, Overclocking Community, dan Technopreneur Community.
							</p>
				            </div>
				        </div>
				    </div>
			  	</div>			

			<div class="row">
				<div class="col-sm-12">
					<div class="block">
						<div class="block-content">
							<h2>Sponsors</h2>
							<img src="{{asset('resources/assets/img/Sponsors.png')}}" class="img-responsive">
					</div>
				</div>
			</div>

			</main>

			<footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
				<div class="pull-left">
				BNCC Hackathon © <span class="js-year-copy"></span>
				</div>
			</footer>
   		</div>

    <script type="text/javascript" src="{{asset('resources/assets/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('resources/assets/js/bootstrap.min.js')}}"></script>
  </body>
</html>
