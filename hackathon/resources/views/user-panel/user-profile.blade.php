
<html>
  <head>
    <title>BNCC Hackathon</title>
	<link rel="icon" href="{{asset('resources/assets/img/logo-hackathon.png')}}">

    <link rel="stylesheet" media="all" href="{{asset('resources/assets/css/admin.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/font-awesome.min.css')}}">
  </head>
  <body>
    	<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
      		<nav id="sidebar">
    			<div id="sidebar-scroll">
        			<div class="sidebar-content">
            			<div class="side-header side-content bg-white-op" style="background-color: white;">
               			<button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                    		<i class="fa fa-times"></i>
                		</button>
		                <a class="h5 text-white" href="{{url('/')}}">
							<img src="{{asset('resources/assets/img/logo-hackathon-small.png')}}" class="img-responsive">
						</a>            
						</div>
	            		<div class="side-content side-content-full">
			                <ul class="nav-main">
			                    <li>
			                      	<a href="{{route('user-information')}}">
			                        	<i class="fa fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Information</span>
									</a>                   
								</li>
			                    <li>
			                     	<a href="{{route('user-profile')}}">
			                        	<i class="fa fa-copy"></i>
			                        	<span class="sidebar-mini-hide">Profile</span>
									</a>
								</li>
			                </ul>
	            		</div>
        			</div>
    			</div>
			</nav>

      		<header id="header-navbar" class="content-mini content-mini-full">

		  	<ul class="nav-header pull-right">
		  		<li>
		    		<a rel="nofollow" data-method="delete" href="{{url('/logout')}}">Log out</a>
		  		</li>
			</ul>
			</header>

			<main id="main-container">

				@if($team->payment == false)
				
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<strong>Still not verified</strong><br><br>Please Complete Payment of {{$team->quantity}} person with the payment of Rp. 
							<b>{{number_format($team->quantity*50000)}}</b>
							to Bank Account BCA (127983712389) ASAP !  
					</div>
				@else

				@endif

		    <div class="content bg-gray-lighter">
			    <div class="row items-push">
			        <div class="col-sm-7">
			            <h1 class="page-heading">
			            	Welcome , Team {{$team->name}} !
			            </h1>
			        </div>
			        <div class="col-sm-5 text-right hidden-xs">
			        	Status : 
			        	@if($team->payment == true)
			        	<span style="color: #2ecc71;">Verified</span>
			        	@else
			        	<span style="color: #e74c3c">Waiting for Payment</span>
			        	@endif
			        </div>
			    </div>
			</div>


			<div class="content">
			  <div class="row">
			    <div class="col-lg-12">
			        <div class="block">
			            <div class="block-content">
			            <h2>Team Members</h2> 
			 			<div class="row" style="margin-top: 20px;">
			 			@foreach($team->users as $user)
							<a data-toggle="modal" href='#{{$user->id}}data' style="text-decoration: none;"><div class="col-sm-4 text-center" style="padding: 20px;">
								<img src="{{asset('resources/assets/img/Developer.png')}}" style="margin: 0 auto;" class="img-responsive">
								<!-- buat desain untuk yang developer,desainer dan bisnis -->
								<h3>{{$user->name}}</h3>
								<h5>{{$user->role}}</h5>
							</div>
							</a>
						@endforeach	
						</div>
			       </div>
			    </div>
			  </div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<div class="block">
						<div class="block-content">
							<h2 style="margin-bottom: 20px;">Rules of Hackathon</h2>
							<p style="text-align: justify;">You can access the following link for information regarding the hackathon rules. You have question , you can send mail to technocareer@bncc.net<br><br><a href="#">bit.ly/Hackathon</a>
							</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="block">
						<div class="block-content">
							<h2 style="margin-bottom: 20px;">Contact Person</h2>
							<div>
								<p style="text-align: justify;">
									Got questions about hackathon? Feel Free to Contact the details below for more information
								</p>
								<h4><i class="fa fa-user-circle"></i>&nbsp;&nbsp;&nbsp;&nbsp;Aldrian Kwan</h4>
								<h4><i class="fa fa-phone"></i>&nbsp;&nbsp;&nbsp;&nbsp;085263654009 (Whatsapp)</h4>
								<h4><i class="fa fa-envelope"></i>&nbsp;&nbsp;&nbsp;&nbsp;technocareer@bncc.net</h4>

							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12">
					<div class="block">
						<div class="block-content">
							<h2>Sponsors</h2>
							<img src="{{asset('resources/assets/img/Sponsors.png')}}" class="img-responsive">
						</div>
					</div>
				</div>
			</div>
	</main>

    <footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
		<div class="pull-left">
	        BNCC Hackathon © <span class="js-year-copy"></span>
	    </div>
	</footer>

    </div>
 
@foreach($team->users as $user)
  	<div class="modal fade" id="{{$user->id}}data">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Data of {{$user->name}}</h4>
				</div>
				<div class="modal-body">
					<table class="table table-striped table-hover text-center">
						<thead">
							<tr>
								<th class="text-center">Information</th>
								<th class="text-center">Information</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Name</td>
								<td>{{$user->name}}</td>
							</tr>
							<tr>
								<td>Gender</td>
								<td>{{$user->gender}}</td>
							</tr>
							<tr>
								<td>Age</td>
								<td>{{$user->age}}</td>
							</tr>
							<tr>
								<td>Email</td>
								<td>{{$user->email}}</td>
							</tr>
							<tr>
								<td>Status</td>
								<td>{{$user->status}}</td>
							</tr>
							<tr>
								<td>Institution</td>
								<td>{{$user->institution}}</td>
							</tr>
							<tr>
								<td>Position</td>
								<td>{{$user->position}}</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	@endforeach


    <script type="text/javascript" src="{{asset('resources/assets/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('resources/assets/js/bootstrap.min.js')}}"></script>
  </body>
</html>
